<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Sky\ActorBundle\Handler;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Campaign Handler 
 * @DI\Service("ecard.namer")
 */
class Namer implements NamerInterface
{
    protected $securityContext;

//    private $surveyManager;

    /**
     * @DI\InjectParams({
     *     "securityContext" = @DI\Inject("security.context", required = false)
     * })
     */
    public function __construct($securityContext)
    {   
        $this->securityContext = $securityContext;
    }


    public function name($object, PropertyMapping $mapping)
    {
        $file = $mapping->getFile($object);
        $name = uniqid();

        if ($extension = $this->getExtension($file)) {
            $name = sprintf('%s.%s', $name, $extension);
        }
        if($mapping->getMappingName()=="document_file")
            return 'doc_'.$name;
        
        if($mapping->getMappingName()=="user_image")
            return 'user_'.$name;
        else
            return null;
    }

    protected function getExtension(UploadedFile $file)
    {
        $originalName = $file->getClientOriginalName();

        if ($extension = pathinfo($originalName, PATHINFO_EXTENSION)) {
            return $extension;
        }

        if ($extension = $file->guessExtension()) {
            return $extension;
        }

        return null;
    }

}