<?php

namespace Sky\ActorBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Doctrine\ORM\EntityRepository;

class AcquireAdmin extends Admin {

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('id')
                ->add('carNumber')
                ->add('cardType')
                ->add('state', null, array(
                    'label' => 'Active'))
                ->add('createdAt', null, array(
                    'label' => 'Date Created'))
                ->add('user', null, array(
                    'label' => 'Created By'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper) {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
//        $user = $securityContext->getToken()->getUser();
        $listMapper
                ->add('carNumber')
                ->add('cardType')
                ->add('Documents', null, array(
                    'template' => 'ActorBundle:Admin:list_pix_preview.html.twig',
                    'attr' => array('style' => 'width: 44.171%; ')
                ))
                ->add('state', null, array('label' => 'Active ?'))
                ->add('createdAt', null, array('label' => 'Date Created'))
                ->add('Expiration date', null, array(
                    'template' => 'ActorBundle:Admin:expiration_date.html.twig',
                    'attr' => array('style' => 'width: 44.171%; ')));
        if ($securityContext->isGranted('ROLE_AGENT')) {
            $listMapper
                    ->add('user', null, array('label' => 'By'))
                    ->add('customUser', null, array('label' => 'Special By'))
                    ->add('updatedAt');
        }
        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
    )))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
//        $user = $securityContext->getToken()->getUser();
        $formMapper
                ->tab('Vehicle information ')
                ->with('General', array('class' => 'col-md-4 success',
                    'box_class' => 'box box-solid box-success',
                ))
                ->add('carNumber', null, array('required' => true))
                ->add('cardType', null, array(
                    'required' => true,
                ))
                ->add('duration', 'choice', array('choices' => array(
                        '3 months' => '3 months',
                        '6 months' => '6 months',
                        '1 year' => '1 year',
            )))
                ->end()
                ->with('Document Upload', array('class' => 'col-md-8 success',
                    'box_class' => 'box box-solid box-success',
                ))
                ->add('document', 'sonata_type_collection', array(
                    'required' => true,
                    'by_reference' => true
                        ), array('edit' => 'inline', 'inline' => 'table'));

        if ($securityContext->isGranted('ROLE_SUPER_ADMIN')) {

            $formMapper
                    ->end()
                    ->end()
                    ->tab('Admin Actions ')
                    ->with('  ', array('class' => 'col-md-6',
                        'box_class' => 'box box-solid box-warning',
                    ))
                    ->add('user', null, array('label' => "Customer/Applicant"))
                    ->add('state', null, array('label' => "Approve"))
                    ->add('approvalDate', 'sonata_type_date_picker', array('required' => false));
        } elseif ($securityContext->isGranted('ROLE_AGENT')) {

            $formMapper
                    ->end()
                    ->end()
                    ->tab('Admin Actions ')
                    ->with('  ', array('class' => 'col-md-6',
                        'box_class' => 'box box-solid box-warning',
                    ))
                    ->add('user', null, array(
                        'required' => true,
                        'label' => "Customer/Applicant",
                        'class' => 'Sky\UserBundle\Entity\User',
                        'help' => "When requesting onbehalf of a customer, select customer here",
                        'query_builder' => function(EntityRepository $er) {
                            return
                                    $er->createQueryBuilder('o')
                                    ->where('o.userType = :userType')
                                    ->andWhere('o.enabled = :n')
                                    ->setParameter('userType', 'Driver')
                                    ->setParameter('n', 1);
                        }
                    ))
                    ->add('state', null, array('label' => "Approve"))
                    ->add('approvalDate', 'sonata_type_date_picker', array('required' => false));
        }
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper) {
        $showMapper
                ->add('id')
                ->add('carNumber')
                ->add('user')
                ->add('type')
                ->add('state')
                ->add('createdAt')
                ->add('updatedAt');
    }

    public function prePersist($data) {
        $data->setUser($this->getUser());
        $this->setFiles($data);
        $this->customerAcquireCreation($data);
        $this->setAllDocs($data);
    }

    public function preUpdate($data) {
//        $data->setUser($this->getUser());
        $this->setFiles($data);
        $this->validateDoc($data);
//        $this->setAllDocs($data);
//        dump($data);
    }

    public function postUpdate($object) {
        $this->setAllDocs($object);
    }

    public function postPersist($object) {
//        parent::prePersist($object);
    }

    public function configure() {
        parent::configure();
        $this->datagridValues['_sort_by'] = 'id';
        $this->datagridValues['_sort_order'] = 'DESC';
    }

    private function customerAcquireCreation($param) {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        if ($securityContext->isGranted('ROLE_AGENT')) {
            $param->setCustomUser($this->getUser());
        }
    }

    public function setFiles($param) {
        foreach ($param->getDocument() as $data) {
            $data->setAcquire($param);
        };
    }

    public function setAllDocs($param) {
        $docs = array();
        foreach ($param->getDocument() as $data) {
            $docs[] = $data->getdocFile();
            $param->setDocFiles($docs);
        };
    }

    public function validateDoc($param) {
        $allValid = array();
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        $approved = $param->getState();
        $ApprovelDate = $param->getApprovaldate();
        $duration = $param->getDuration();



        foreach ($param->getDocument() as $data) {
            $allValid[] = $data->getvalidity() ? 1 : 'Invalid';
        };

        // expiration date shpuld be customizable buy only Super-Admin
        if ($securityContext->isGranted('ROLE_AGENT')) {
            //send sms when user has access to 
            //all document are valid and approved
            //Update the Expiration Date
            if (!in_array('Invalid', $allValid) && $approved && $duration) {
                // all has been said as valid and approved notify Applicant and expiration Date
                $setExpire = $ApprovelDate ? $param->setExpirationDate(date_add($ApprovelDate, date_interval_create_from_date_string($duration))) : $this->getRequest()->getSession()->getFlashBag()->add("error", "Set Approval Date to complete Acquisition Process");
                $setExpire? null:$param->setState(null) ;
            } else {
                
            }
        }
    }

    public function getUser() {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        $user = $securityContext->getToken()->getUser();
        return $user;
    }

    public function createQuery($context = 'list') {

        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $query = parent::createQuery($context);

        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');

        if (!$securityContext->isGranted('ROLE_AGENT')) {
            //drivers only see their respected created Items

            $query = $this->getModelManager()->createQuery($this->getClass(), 'u');
            $query
                    ->where('u.user = :user')
                    ->setParameter('user', $user);
            return $query;
        }

        return $query;
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'ActorBundle:Admin:acquire_edit.html.twig';
                break;
//            case 'list':
//                return 'CoreBundle:Admin:partnership_list.html.twig';
//                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

}
