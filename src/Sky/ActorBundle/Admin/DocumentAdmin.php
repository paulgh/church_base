<?php

namespace Sky\ActorBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class DocumentAdmin extends Admin {

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('acquire', null, array('label' => 'Car Number'))
                ->add('docType')
                ->add('validity')

        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('acquire', null, array('label' => 'Car Number'))
                ->add('File', null, array(
//                    'template' => 'UserBundle:Admin:list_pix_preview.html.twig',
                    'attr' => array('style' => 'width: 44.171%; ')
                        )
                )
                ->add('docType', null, array(
                    'label' => 'doc Type',
                    'multiple' => true,
                    'required' => true
                ))
                ->add('validity', null, array(
                    'label' => 'User  Abilities and Roles',
                    'multiple' => true,
                ))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        $user = $securityContext->getToken()->getUser();
        $fileFieldOptions = array();
        if (null != $this->getSubject()) {
            $image = $this->getSubject()->getdocFile();
            // use $fileFieldOptions so we can add other options to the field
            $fileFieldOptions = array('required' => false);
            if ($image) {
                // get the container so the full path to the image can be set
                $container = $this->getConfigurationPool()->getContainer();
                $fullPath = $container->get('request')->getBasePath() . '/uploads/document/' . $image;

                // add a 'help' option containing the preview's img tag
                $fileFieldOptions['help'] = $this->uploadFileExtension($fullPath) ?
                        $image . '<br> <a  class="fancybox" href="' . $fullPath . '">'
                        . '<img id="editImage" width="100px" src="' . $fullPath . '" class="admin-preview" /></a>' :
                        $image . ' <br> <a target="_blank" class="fancybox" href="' . $fullPath . '">click to open'
                        . '</a>';
            }
        }
        $formMapper
                ->add('imageFile', 'file', array(
                    'label' => 'Upload Document',
                    'by_reference' => false,
                    'data_class' => null,
                    'required' => true,
                    'required' => false), $fileFieldOptions
                )
                ->add('docFile', null, ['required' => true,
                    'required' => FALSE,
                    'attr' => ["class" => "inLineLabel"],
                    'label'=>' File Name ',
                    'disabled' => 1,
                    'read_only' => 1
                    ])
                ->add('docType', null, ['required' => true])
                ;
        if ($securityContext->isGranted('ROLE_AGENT')) {
            $formMapper
                    ->add('validity', null, array(
            ));
        } else {
            $formMapper
                    ->add('validity', null, array(
                        'label'=>"Approve",
                        'disabled' => 1,
                        'read_only' => True
                        )
            );
        };
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper) {

        if (null != $this->getSubject()) {
            $image = $this->getSubject()->getdocFile();
            // use $fileFieldOptions so we can add other options to the field
            $fileFieldOptions = array('required' => false);
            if ($image) {
                // get the container so the full path to the image can be set
                $container = $this->getConfigurationPool()->getContainer();
                $fullPath = $container->get('request')->getBasePath() . '/uploads/document/' . $image;

                // add a 'help' option containing the preview's img tag
                $fileFieldOptions['help'] = $this->uploadFileExtension($fullPath) ?
                        $image . '<br> <a  class="fancybox" href="' . $fullPath . '">'
                        . '<img id="editImage" width="100px" src="' . $fullPath . '" class="admin-preview" /></a>' :
                        $image . ' <br> <a target="_blank" class="fancybox" href="' . $fullPath . '">click to open'
                        . '</a>';
            }
        }
        $showMapper
                ->add('imageFile', 'file', array(
                    'label' => 'Profile Picture',
                    'by_reference' => false,
                    'data_class' => null,
                    'required' => true), $fileFieldOptions
                )
                ->add('docType')
                ->add('validity')
        ;
    }

    public function uploadFileExtension($path) {
        $fileExtension = pathinfo($path, PATHINFO_EXTENSION);

        $images = array('png', 'gif', 'jpeg', 'jpg');

        if (in_array($fileExtension, $images) !== false) {
            return True;
        }
        return FALSE;
    }

}
