<?php

namespace Sky\ActorBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TicketAdmin extends Admin {
    
    
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('movie')
                ->add('refCode')
                ->add('purchaseSource')
                ->add('redeemed')
                ->add('mobileNo')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('refCode')
                ->add('movie')
                ->add('movie.price')
                ->add('amountPaid')
                ->add('discount')
                ->add('quantity', null, ['label' => 'Qty Purchased'])
                ->add('used', null, ['label' => 'Qty used','editable'=>true])
                ->add('purchaseSource')
                ->add('redeemed',null,['editable'=>true])
                ->add('mobileNo')
                ->add('createdAt', null, ['label' => 'date Purchased'])
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('movie')
                ->add('quantity')
//                ->add('purchaseSource')
                ->add('redeemed')

        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper) {
        $showMapper;
    }

    public function prePersist($data) {
        $data->setUser($this->getUser());
        $this->setTicketDetails($data);
        $this->setrefCode($data);
    }

    public function preUpdate($data) {
        $this->setTicketDetails($data);
    }

    public function getUser() {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        $user = $securityContext->getToken()->getUser();
        return $user;
    }

    public function setTicketDetails($param) {

        $price = $param->getMovie()->getPrice();
        $discount = $param->getMovie()->getDiscount();
        $qty = $param->getQuantity();
        $amountPaid = ($price * $qty) - ($price * $discount * $qty);

        $param->setPurchaseSource('Venue');
        $param->setAmountPaid($amountPaid);
        $param->setDiscount($price * $discount * $qty);
    }

    public function setrefCode($param) {
        $param->setRefCode(uniqid());
    }

}
