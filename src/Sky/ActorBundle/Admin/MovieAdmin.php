<?php

namespace Sky\ActorBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

//use blackknight467\StarRatingBundle\Form\RatingType;

class MovieAdmin extends Admin {

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('duration')
                ->add('title')
                ->add('state')
                ->add('showingDate')
                ->add('createdAt', null, array(
                    'label' => 'Date Created'))
                ->add('user', null, array(
                    'label' => 'Created By'));
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('Poster', null, array(
                    'template' => 'ActorBundle:Admin:movie.html.twig',
                    'attr' => array('style' => 'width: 44.171%; ')
                        )
                )
                ->add('title')
                ->add('price')
                ->add('showingDate')
                ->add('duration')
                ->add('discount', 'percent', array())
                ->add('state', null, array(
                    'label' => "Active"
                ))
                ->add('createdAt', null, array(
                    'label' => 'Date Created'))
                ->add('user', null, array(
                    'label' => 'Created By'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        $user = $securityContext->getToken()->getUser();
        $fileFieldOptions = array();
        if (null != $this->getSubject()) {
            $image = $this->getSubject()->getMovImage();
            // use $fileFieldOptions so we can add other options to the field
            $fileFieldOptions = array('required' => false);
            if ($image) {
                // get the container so the full path to the image can be set
                $container = $this->getConfigurationPool()->getContainer();
                $fullPath = $container->get('request')->getBasePath() . '/uploads/movie/' . $image;

                // add a 'help' option containing the preview's img tag
                $fileFieldOptions['help'] = $image . '<br> <a  class="fancybox" href="' . $fullPath . '">'
                        . '<img id="editImage" width="100px" src="' . $fullPath . '" class="admin-preview" /></a>';
            }
        }
        $formMapper
                ->with('Movie Detail', array('class' => 'col-md-6 success',
                    'box_class' => 'box box-solid box-success',
                ))
                ->add('title', null, array('required' => true))
                ->add('Category', null, array(
                    'required' => true,
                ))
                ->add('duration', null, array(
                    'help' => 'Running time in Mins')
                )
                ->add('rating', null, array(
                    'label' => 'Movie Star rating')
                )
                ->add('shortDescription', null, array(
                ))
                ->add('imageFile', 'file', array(
                    'label' => 'Upload Movie Poster',
                    'by_reference' => false,
                    'data_class' => null,
                    'required' => true,
                    'required' => false), $fileFieldOptions
                )
                ->end()
                ->with('Cinema Detail', array('class' => 'col-md-6 success',
                    'box_class' => 'box box-solid box-success',
                ))
                ->add('price', null, array(
                    'help' => 'Price for the Movie Ghc')
                )
                ->add('discount', 'percent', array(
                    'help' => 'Discount for the movie')
                )
                ->add('audience', null, array(
                    'help' => 'The MPAA film ratings for age',
                ))
                ->add('showingDate', "sonata_type_date_picker", array(
                    'help' => 'Specify date movie will be showing'))
                ->add('state', null, array(
                    'help' => 'Activate to mark on front Page',
                    'label' => "Active"
                ))
                ->add('previewVideoLink', null, array(
                    'help' => 'place video Link here')
                )
                ->add('teaser', null, array(
                    'help' => 'select to mark for Trending movies')
                )
                ->add('showingTime', null, array(
                    'help' => 'Trending showing place and time')
                )
                ->end()
                ->end()
                ->with('Cinema Showing', array('class' => 'col-md-12 warning',
                    'box_class' => 'box box-solid box-warning',
                ))
                ->add('showing', 'sonata_type_collection', array(
                    'required' => true,
                    'label' => 'Add cinema Rooms',
                    'by_reference' => true
                        ), array('edit' => 'inline', 'inline' => 'table'));
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper) {
        $showMapper
                ->add('id')
                ->add('duration')
                ->add('title')
                ->add('state')
                ->add('showingDate')
                ->add('movImage')
                ->add('createdAt')
                ->add('updatedAt')
                ->add('deletedAt')
        ;
    }

    public function setRooms($param) {
        foreach ($param->getShowing() as $data) {
            $data->setMovie($param);
        };
    }

    public function prePersist($object) {
        $this->setRooms($object);
    }

    public function preUpdate($object) {
        $this->setRooms($object);
    }

    public function getUser() {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        $user = $securityContext->getToken()->getUser();
        return $user;
    }

}
