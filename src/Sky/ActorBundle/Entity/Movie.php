<?php

namespace Sky\ActorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Sky\UserBundle\Model\UserInterface;
use Sky\UserBundle\Model\AgentInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Accessor;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\Collection;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="\Sky\ActorBundle\Repository\MovieRepository")
 * @ORM\Table(name="movie")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")
 * @UniqueEntity("title",message="Title  of Movie already exist ")
 */
class Movie {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(name="duration", type="string")
     * @Expose
     */
    private $duration;
    
    /**
     * @ORM\Column(name="showing_time", type="text",nullable=true)
     * @Expose
     */
    private $showingTime;

    /**
     * @ORM\Column(name="title", type="string")
     * @Expose
     */
    private $title;

    /**
     * @ORM\Column(name="state", type="boolean",nullable=true)
     * @Expose
     */
    private $state;

    /**
     * @ORM\Column(name="teaser", type="boolean",nullable=true)
     * @Expose
     */
    private $teaser;
    
    /**
     * @ORM\Column(name="price", type="float")
     * @Expose
     */
    private $price;
    
    /**
     * @ORM\Column(name="discount", type="float")
     * @Expose
     */
    private $discount;
    
     /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="\Sky\SettingsBundle\Entity\Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     * 
     */
    private $Category;
    
     /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="\Sky\SettingsBundle\Entity\Audience")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="audience_id", referencedColumnName="id")
     * })
     * 
     */
    private $audience;
    
    /**
     *  @ORM\OneToMany(targetEntity="\Sky\SettingsBundle\Entity\Showing", mappedBy="movie", orphanRemoval=true, cascade={"persist"})
     */
    private $showing;    
    
    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="showing_date", type="date", nullable=true)
     */
    private $showingDate;
    
    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="preview_video_link", type="string", nullable=true)
     */
    private $previewVideoLink;
    

    /**
     * @ORM\Column(name="mov_image", type="string", length=255, nullable=true)
     * @Expose
     */
    private $movImage;
    
    /**
     * @ORM\Column(name="short_description", type="text", nullable=true)
     * @Expose
     */
    private $shortDescription;
    
    
    
    /**
     * @ORM\Column(name="rating", type="string", nullable=true)
     * @Expose
     */
    private $rating;
    
    

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="mov_file", fileNameProperty="movImage")
     * 
     * @var File
     */
    private $imageFile;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Commodity
     */
    public function setImageFile(File $image = null) {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile() {
        return $this->imageFile;
    }

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

  

    /**
     *
     * @ORM\ManyToOne(targetEntity="\Sky\UserBundle\Entity\User",inversedBy="actor", inversedBy="actor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    public function __toString() {
        return (string) $this->title;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->showing = new \Doctrine\Common\Collections\ArrayCollection();

        }

    


    /**
     * Set duration
     *
     * @param string $duration
     * @return Movie
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set showingTime
     *
     * @param string $showingTime
     * @return Movie
     */
    public function setShowingTime($showingTime)
    {
        $this->showingTime = $showingTime;

        return $this;
    }

    /**
     * Get showingTime
     *
     * @return string 
     */
    public function getShowingTime()
    {
        return $this->showingTime;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Movie
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set showingToday
     *
     * @param boolean $showingToday
     * @return Movie
     */
    public function setShowingToday($showingToday)
    {
        $this->showingToday = $showingToday;

        return $this;
    }

    /**
     * Get showingToday
     *
     * @return boolean 
     */
    public function getShowingToday()
    {
        return $this->showingToday;
    }

    /**
     * Set showingDate
     *
     * @param \DateTime $showingDate
     * @return Movie
     */
    public function setShowingDate($showingDate)
    {
        $this->showingDate = $showingDate;

        return $this;
    }

    /**
     * Get showingDate
     *
     * @return \DateTime 
     */
    public function getShowingDate()
    {
        return $this->showingDate;
    }

    /**
     * Set movImage
     *
     * @param string $movImage
     * @return Movie
     */
    public function setMovImage($movImage)
    {
        $this->movImage = $movImage;

        return $this;
    }

    /**
     * Get movImage
     *
     * @return string 
     */
    public function getMovImage()
    {
        return $this->movImage;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Movie
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Movie
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Movie
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set Category
     *
     * @param \Sky\SettingsBundle\Entity\Category $category
     * @return Movie
     */
    public function setCategory(\Sky\SettingsBundle\Entity\Category $category = null)
    {
        $this->Category = $category;

        return $this;
    }

    /**
     * Get Category
     *
     * @return \Sky\SettingsBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->Category;
    }

    /**
     * Set user
     *
     * @param \Sky\UserBundle\Entity\User $user
     * @return Movie
     */
    public function setUser(\Sky\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sky\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Movie
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set discount
     *
     * @param float $discount
     * @return Movie
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return float 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     * @return Movie
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string 
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set rating
     *
     * @param string $rating
     * @return Movie
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return string 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set audience
     *
     * @param \Sky\SettingsBundle\Entity\Audience $audience
     * @return Movie
     */
    public function setAudience(\Sky\SettingsBundle\Entity\Audience $audience = null)
    {
        $this->audience = $audience;

        return $this;
    }

    /**
     * Get audience
     *
     * @return \Sky\SettingsBundle\Entity\Audience 
     */
    public function getAudience()
    {
        return $this->audience;
    }

    /**
     * Set previewVideoLink
     *
     * @param string $previewVideoLink
     * @return Movie
     */
    public function setPreviewVideoLink($previewVideoLink)
    {
        $this->previewVideoLink = $previewVideoLink;

        return $this;
    }

    /**
     * Get previewVideoLink
     *
     * @return string 
     */
    public function getPreviewVideoLink()
    {
        return $this->previewVideoLink;
    }

    /**
     * Set state
     *
     * @param boolean $state
     * @return Movie
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return boolean 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Add showing
     *
     * @param \Sky\SettingsBundle\Entity\Showing $showing
     * @return Movie
     */
    public function addShowing(\Sky\SettingsBundle\Entity\Showing $showing)
    {
        $this->showing[] = $showing;

        return $this;
    }

    /**
     * Remove showing
     *
     * @param \Sky\SettingsBundle\Entity\Showing $showing
     */
    public function removeShowing(\Sky\SettingsBundle\Entity\Showing $showing)
    {
        $this->showing->removeElement($showing);
    }

    /**
     * Get showing
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getShowing()
    {
        return $this->showing;
    }

    /**
     * Set teaser
     *
     * @param boolean $teaser
     * @return Movie
     */
    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;

        return $this;
    }

    /**
     * Get teaser
     *
     * @return boolean 
     */
    public function getTeaser()
    {
        return $this->teaser;
    }
}
