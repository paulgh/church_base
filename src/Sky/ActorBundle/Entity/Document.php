<?php

namespace Sky\ActorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @Vich\Uploadable
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 * @ORM\Table(name="document")
 * @ORM\Entity()
 */
class Document {

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Pledge
     * @ORM\ManyToOne(targetEntity="\Sky\ActorBundle\Entity\Acquire",inversedBy="actorMything",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="acquire_id", referencedColumnName="id",nullable=true)
     * }))
     */
    private $acquire;

    /**
     * @ORM\Column(name="doc_file", type="string", length=255, nullable=true)
     * @Expose
     */
    private $docFile;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="document_file", fileNameProperty="docFile")
     * 
     * @var File
     */
    private $imageFile;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Commodity
     */
    public function setImageFile(File $image = null) {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile() {
        return $this->imageFile;
    }
    
    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Sky\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     * 
     */
    private $user;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="\Sky\SettingsBundle\Entity\DocumentType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="doc_type_id", referencedColumnName="id")
     * })
     * 
     */
    private $docType;

    /**
     * @ORM\Column(name="validity", type="boolean",nullable=true)
     */
    private $validity;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return MyThing
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return MyThing
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return MyThing
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    /**
     * Set validity
     *
     * @param boolean $validity
     * @return Document
     */
    public function setValidity($validity) {
        $this->validity = $validity;

        return $this;
    }

    /**
     * Get validity
     *
     * @return boolean 
     */
    public function getValidity() {
        return $this->validity;
    }

    /**
     * Set acquire
     *
     * @param \Sky\ActorBundle\Entity\Acquire $acquire
     * @return Document
     */
    public function setAcquire(\Sky\ActorBundle\Entity\Acquire $acquire = null) {
        $this->acquire = $acquire;

        return $this;
    }

    /**
     * Get acquire
     *
     * @return \Sky\ActorBundle\Entity\Acquire 
     */
    public function getAcquire() {
        return $this->acquire;
    }

    /**
     * Set user
     *
     * @param \Sky\UserBundle\Entity\User $user
     * @return Document
     */
    public function setUser(\Sky\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sky\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }


    /**
     * Set docType
     *
     * @param \Sky\SettingsBundle\Entity\DocumentType $docType
     * @return Document
     */
    public function setDocType(\Sky\SettingsBundle\Entity\DocumentType $docType = null)
    {
        $this->docType = $docType;

        return $this;
    }

    /**
     * Get docType
     *
     * @return \Sky\SettingsBundle\Entity\DocumentType 
     */
    public function getDocType()
    {
        return $this->docType;
    }
    
    

    /**
     * Set docFile
     *
     * @param string $docFile
     * @return Document
     */
    public function setDocFile($docFile)
    {
        $this->docFile = $docFile;

        return $this;
    }

    /**
     * Get docFile
     *
     * @return string 
     */
    public function getDocFile()
    {
        return $this->docFile;
    }
}
