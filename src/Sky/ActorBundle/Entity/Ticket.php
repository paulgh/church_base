<?php

namespace Sky\ActorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Sky\UserBundle\Model\UserInterface;
use Sky\UserBundle\Model\AgentInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Accessor;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\Collection;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="\Sky\ActorBundle\Repository\MovieRepository")
 * @ORM\Table(name="ticket")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")
 * @UniqueEntity("refCode",message="ref Code Error of Movie already exist ")
 */
class Ticket {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(name="discount", type="float")
     * @Expose
     */
    private $discount;

    /**
     * @ORM\Column(name="amount_paid", type="float")
     * @Expose
     */
    private $amountPaid;

    /**
     * @ORM\Column(name="quantity", type="integer")
     * @Expose
     */
    private $quantity;

    /**
     * @ORM\Column(name="ref_code", type="string", nullable=true)
     * @Expose
     */
    private $refCode;
    /**
     * @ORM\Column(name="used", type="integer", nullable=true)
     * @Expose
     */
    private $used;

    /**
     * @ORM\Column(name="purchase_source", type="string", nullable=true)
     * @Expose
     */
    private $purchaseSource;
    
    /**
     * @ORM\Column(name="mobileNo", type="string", nullable=true)
     * @Expose
     */
    private $mobileNo;

    /**
     * @ORM\Column(name="redeemed", type="boolean", nullable=true)
     * @Expose
     */
    private $redeemed;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\Sky\UserBundle\Entity\User",inversedBy="actor", inversedBy="actor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\Sky\ActorBundle\Entity\Movie",inversedBy="actor", inversedBy="actor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
     * })
     */
    private $movie;

    public function __toString() {
        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct() {
        
    }

    /**
     * Set discount
     *
     * @param float $discount
     * @return Ticket
     */
    public function setDiscount($discount) {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return float 
     */
    public function getDiscount() {
        return $this->discount;
    }

    /**
     * Set amountPaid
     *
     * @param float $amountPaid
     * @return Ticket
     */
    public function setAmountPaid($amountPaid) {
        $this->amountPaid = $amountPaid;

        return $this;
    }

    /**
     * Get amountPaid
     *
     * @return float 
     */
    public function getAmountPaid() {
        return $this->amountPaid;
    }

    /**
     * Get quantity
     *
     * @return \number 
     */
    public function getQuantity() {
        return $this->quantity;
    }

    /**
     * Set purchaseSource
     *
     * @param string $purchaseSource
     * @return Ticket
     */
    public function setPurchaseSource($purchaseSource) {
        $this->purchaseSource = $purchaseSource;

        return $this;
    }

    /**
     * Get purchaseSource
     *
     * @return string 
     */
    public function getPurchaseSource() {
        return $this->purchaseSource;
    }

    /**
     * Set redeemed
     *
     * @param boolean $redeemed
     * @return Ticket
     */
    public function setRedeemed($redeemed) {
        $this->redeemed = $redeemed;

        return $this;
    }

    /**
     * Get redeemed
     *
     * @return boolean 
     */
    public function getRedeemed() {
        return $this->redeemed;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Ticket
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Ticket
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Ticket
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    /**
     * Set user
     *
     * @param \Sky\UserBundle\Entity\User $user
     * @return Ticket
     */
    public function setUser(\Sky\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sky\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set refCode
     *
     * @param string $refCode
     * @return Ticket
     */
    public function setRefCode($refCode) {
        $this->refCode = $refCode;

        return $this;
    }

    /**
     * Get refCode
     *
     * @return string 
     */
    public function getRefCode() {
        return $this->refCode;
    }

    /**
     * Set movie
     *
     * @param \Sky\ActorBundle\Entity\Movie $movie
     * @return Ticket
     */
    public function setMovie(\Sky\ActorBundle\Entity\Movie $movie = null) {
        $this->movie = $movie;

        return $this;
    }

    /**
     * Get movie
     *
     * @return \Sky\ActorBundle\Entity\Movie 
     */
    public function getMovie() {
        return $this->movie;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Ticket
     */
    public function setQuantity($quantity) {
        $this->quantity = $quantity;

        return $this;
    }


    /**
     * Set mobileNo
     *
     * @param string $mobileNo
     * @return Ticket
     */
    public function setMobileNo($mobileNo)
    {
        $this->mobileNo = $mobileNo;

        return $this;
    }

    /**
     * Get mobileNo
     *
     * @return string 
     */
    public function getMobileNo()
    {
        return $this->mobileNo;
    }

    /**
     * Set used
     *
     * @param integer $used
     * @return Ticket
     */
    public function setUsed($used)
    {
        $this->used = $used;

        return $this;
    }

    /**
     * Get used
     *
     * @return integer 
     */
    public function getUsed()
    {
        return $this->used;
    }
}
