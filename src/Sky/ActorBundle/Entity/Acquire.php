<?php

namespace Sky\ActorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Sky\UserBundle\Model\UserInterface;
use Sky\UserBundle\Model\AgentInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Accessor;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\Collection;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="\Sky\ActorBundle\Repository\ActorRepository")
 * @ORM\Table(name="acquire")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")
 * @UniqueEntity("carNumber",message="car number already exist ")
 */
class Acquire {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(name="duration", type="string",nullable=false)
     * @Expose
     */
    private $duration;

    /**
     * @ORM\Column(name="car_number", type="string",nullable=false)
     * @Expose
     */
    private $carNumber;

    /**
     * @ORM\Column(name="state", type="boolean",nullable=true)
     * @Expose
     */
    private $state;

    /**
     * @ORM\Column(name="doc_files", type="text",nullable=true)
     * @Expose
     */
    private $docFiles;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="expiration_date", type="date", nullable=true)
     */
    private $expirationDate;
    
    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="app_date", type="date", nullable=true)
     */
    private $approvalDate;

    /**
     *
     *  @ORM\OneToMany(targetEntity="\Sky\ActorBundle\Entity\Document", mappedBy="acquire", orphanRemoval=true, cascade={"persist"})
     */
    private $document;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\Sky\SettingsBundle\Entity\CardType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="card_type_id", referencedColumnName="id")
     * })
     */
    private $cardType;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\Sky\UserBundle\Entity\User",inversedBy="actor", inversedBy="actor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\Sky\UserBundle\Entity\User",inversedBy="actor", inversedBy="actor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="custom_user_id", referencedColumnName="id")
     * })
     */
    private $customUser;

    public function __toString() {
        return (string) $this->carNumber;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->document = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set duration
     *
     * @param string $duration
     * @return Acquire
     */
    public function setDuration($duration) {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string 
     */
    public function getDuration() {
        return $this->duration;
    }

    /**
     * Set carNumber
     *
     * @param string $carNumber
     * @return Acquire
     */
    public function setCarNumber($carNumber) {
        $this->carNumber = $carNumber;

        return $this;
    }

    /**
     * Get carNumber
     *
     * @return string 
     */
    public function getCarNumber() {
        return $this->carNumber;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Acquire
     */
    public function setState($state) {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState() {
        return $this->state;
    }

    /**
     * Set expirationDate
     *
     * @param \DateTime $expirationDate
     * @return Acquire
     */
    public function setExpirationDate($expirationDate) {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * Get expirationDate
     *
     * @return \DateTime 
     */
    public function getExpirationDate() {
        return $this->expirationDate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Acquire
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Acquire
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Acquire
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    /**
     * Add document
     *
     * @param \Sky\ActorBundle\Entity\Document $document
     * @return Acquire
     */
    public function addDocument(\Sky\ActorBundle\Entity\Document $document) {
        $this->document[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \Sky\ActorBundle\Entity\Document $document
     */
    public function removeDocument(\Sky\ActorBundle\Entity\Document $document) {
        $this->document->removeElement($document);
    }

    /**
     * Get document
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Set user
     *
     * @param \Sky\UserBundle\Entity\User $user
     * @return Acquire
     */
    public function setUser(\Sky\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sky\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set cardType
     *
     * @param \Sky\SettingsBundle\Entity\CardType $cardType
     * @return Acquire
     */
    public function setCardType(\Sky\SettingsBundle\Entity\CardType $cardType = null) {
        $this->cardType = $cardType;

        return $this;
    }

    /**
     * Get cardType
     *
     * @return \Sky\SettingsBundle\Entity\CardType 
     */
    public function getCardType() {
        return $this->cardType;
    }

    /**
     * Set customUser
     *
     * @param \Sky\UserBundle\Entity\User $customUser
     * @return Acquire
     */
    public function setCustomUser(\Sky\UserBundle\Entity\User $customUser = null) {
        $this->customUser = $customUser;

        return $this;
    }

    /**
     * Get customUser
     *
     * @return \Sky\UserBundle\Entity\User 
     */
    public function getCustomUser() {
        return $this->customUser;
    }

    /**
     * Set docFiles
     *
     * @param string $docFiles
     * @return Acquire
     */
    public function setDocFiles($docFiles) {
        $this->docFiles = $docFiles;
                
        return $this;
    }

    /**
     * Get docFiles
     *
     * @return string 
     */
    public function getDocFiles() {

        $docs = array();

        foreach ($this->getDocument() as $data) {
            $docs[] = $data->getdocFile();
        };
        $all = implode(",", $docs);
        
        $this->setDocFiles($all);
        return $this->docFiles;
    }


  

    /**
     * Set approvalDate
     *
     * @param \DateTime $approvalDate
     * @return Acquire
     */
    public function setApprovalDate($approvalDate)
    {
        $this->approvalDate = $approvalDate;

        return $this;
    }

    /**
     * Get approvalDate
     *
     * @return \DateTime 
     */
    public function getApprovalDate()
    {
        return $this->approvalDate;
    }
}
