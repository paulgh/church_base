<?php

namespace Sky\ActorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ActorBundle:Default:index.html.twig', array('name' => $name));
    }
}
