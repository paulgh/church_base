<?php

namespace Sky\ActorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Ddeboer\DataImport\Workflow;
use Ddeboer\DataImport\Reader\CsvReader;
use Ddeboer\DataImport\Writer\CsvWriter;
use Ddeboer\DataImport\Writer\ExcelWriter;
use Ddeboer\DataImport\Writer\DoctrineWriter;
use Ddeboer\DataImport\ValueConverter\StringToDateTimeValueConverter;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sky\ActorBundle\Form\ActorImportType;
use Sky\ActorBundle\Entity\Actor;
use Sky\LocationBundle\Entity\Region;
use Sky\LocationBundle\Entity\Town;
use Sky\SettingsBundle\Entity\AgeRange;
use Sky\SettingsBundle\Entity\DeviceMechanism;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\DateTime;

class ImportActorController extends Controller {

    public function listAction(Request $request) {


        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $actorEntity = new Actor();
//        $regionEntity = new Region();
        $townEntity = new Town();
        $deviceMechnismEntity = new DeviceMechanism();
        $ageRangeEntity = new AgeRange();

        $importForm = $this->createCreateForm($actorEntity, 'actor_upload_list');
        $where = "";
        $user = $this->getUser();

        $importForm->handleRequest($request);
        if ($importForm->isValid()) {

            $file = $_FILES["sky_actorbundle_actorimport"]['tmp_name']['upload'];
            $file = new \SplFileObject($file);
            $reader = new CsvReader($file);
            $reader->setHeaderRowNumber(0);
            $count = 0;



            foreach ($reader as $row) {
                if (isset($row['name']) != "") {

                    isset($row['name']) ? $actorEntity->setFullName($row['name']) : null;
                    isset($row['age']) ? $actorEntity->setAge($row['age']) : null;
                    isset($row['mobile_number']) ? $actorEntity->setMobileNo($row['mobile_number']) : null;
                    isset($row['number_src']) ? $actorEntity->setContactSrc($row['number_src']) : null;
                    isset($row['not_my_thing']) ? $actorEntity->setNotMyThingData($row['not_my_thing']) : null;
                    isset($row['my_thing']) ? $actorEntity->setMyThingData($row['my_thing']) : null;

                    isset($row['whatsapp']) ? $actorEntity->setWhatsapp(ucfirst($row['whatsapp'])) : null;
                    isset($row['sms']) ? $actorEntity->setSms(ucfirst($row['sms'])) : null;

                    $nmt = isset($row['not_my_thing']) ? $row['not_my_thing'] : null;
                    $mt = isset($row['my_thing']) ? $row['my_thing'] : null;


                    $town = isset($row['location']) ? $this->getDoctrine()->getRepository("LocationBundle:Town")->findUnitByName(($row['location'])) : null;
                    $ageRange = isset($row['age_range']) ? $this->getDoctrine()->getRepository("SettingsBundle:AgeRange")->findUnitByName(($row['age_range'])) : null;


                    $myThing = explode(',', $mt);
                    $notMyThing = explode(',', $nmt);

                    if (empty($town) && !empty($row['location'])) {
//                        // insert and fetch it
                        $townEntity->setName(ucfirst(strtolower($row['location'])));
                        $townEntity->setCatchment(1);
                        $townEntity->setCreatedAt(new \DateTime(date('Y-m-d H:i:s', time())));
                        $townEntity->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s', time())));
                        $this->saveMessage($townEntity);
                        $town = $this->getDoctrine()->getRepository("LocationBundle:Town")->findUnitByName(($row['location']));
                    }
                    if (empty($ageRange) && !empty($row['age_range'])) {
//                        // insert and fetch it
                        $ageRangeEntity->setRange($row['age_range']);
                        $ageRangeEntity->setCreatedAt(new \DateTime(date('Y-m-d H:i:s', time())));
                        $ageRangeEntity->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s', time())));
                        $this->saveMessage($ageRangeEntity);
                        $ageRange = $this->getDoctrine()->getRepository("SettingsBundle:AgeRange")->findUnitByName(($row['age_range']));
                    }

                    $town['id'] ? $actorEntity->setTown($this->getTown($town['id'])) : $actorEntity->setTown(null);
                    $ageRange['id'] ? $actorEntity->setAgeRange($this->getAgeRange($ageRange['id'])) : $actorEntity->setAgeRange(null);
//                    $actorEntity->setUser($user);
                    //device Mechanism
                    $insertId = $this->saveMessage2($actorEntity);

                    if ($insertId) {
                        //Device Mechanism
                        $notMyThingDm = $this->myDeviceMech($notMyThing);
                        $myThingDm = $this->myDeviceMech($myThing);

                        if (!empty($myThingDm)) {
                            foreach ($myThingDm as $k => $v) {
                                $data = $this->insertDevice($insertId, $v, 'my_thing');
//                                dump($data);
                            };
                        }

                        if (!empty($notMyThingDm)) {
                            foreach ($notMyThingDm as $k => $v) {
                                $data = $this->insertDevice($insertId, $v, 'not_my_thing');
//                                dump($data);
                            }
                        }
                    }

                    $this->saveMessage($actorEntity);
                    $em->clear();
                }

                $count += 1;
            }

//            exit;

            if ($count > 0) {
                $this->getRequest()->getSession()->getFlashBag()->add("minagri_success", "Operation successful! " . $count . " Girls saved!! ");
                return new RedirectResponse($this->generateUrl('actor_upload_list'));
            } else {
                $this->get('session')->getFlashBag()->set("minagri_error", "Operation failed! ");
                return new RedirectResponse($this->generateUrl('actor_upload_list'));
            }
        }


        return $this->render('ActorBundle:Default:upload_actor.html.twig', array(
                    'importForm' => $importForm->createView(),
                    'admin_pool' => $this->container->get('sonata.admin.pool'),
                    'blocks' => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
                        )
        );
    }

    // function to create form  deleteeditVoiceCampaign
    private function createCreateForm(Actor $entity, $url = "") {

        $er = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository('UserBundle:User');

        $form = $this->createForm(new ActorImportType($this->getUser(), $er), $entity, array(
            'action' => $this->generateUrl($url),
            'method' => 'POST',
        ));
        return $form;
    }

//    $this->getEntityId('Name', $row['commodity'], 'SettingBundle:Commodity', $em,$user);

    public function createNewEntry($params, $fieldName, $nameSpace, $em, $user = false) {
        $paramName = 'set' . $params;
        $createEntity = new $nameSpace();
        $createEntity->$paramName($fieldName);
        $createEntity->setCreatedAt(new \DateTime(date('Y-m-d H:i:s', time())));
        $createEntity->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s', time())));

        if ($user != null) {
            $createEntity->setUser($user);
        }
        $em->persist($createEntity);
        $em->flush();

        return $createEntity;
    }

    public function getUserInfo($id) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('Sky\UserBundle\Entity\User')->find($id);
    }

    public function getRegion($id) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('Sky\LocationBundle\Entity\Region')->find($id);
    }

    public function getTown($id) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('Sky\LocationBundle\Entity\Town')->find($id);
    }

    public function getAgeRange($id) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('Sky\SettingsBundle\Entity\AgeRange')->find($id);
    }

    public function getMyDeviceId($id) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('Sky\SettingsBundle\Entity\DeviceMechanism')->find($id);
    }

    public function getMembership($id) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('Sky\SettingBundle\Entity\MembershipStatus')->find($id);
    }

    public function getRelation($id) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('Sky\SettingBundle\Entity\Relation')->find($id);
    }

    public function getCurrentOffice($id) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('Sky\SettingBundle\Entity\CurrentOffice')->find($id);
    }

    public function getProfession($id) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('Sky\SettingBundle\Entity\Profession')->find($id);
    }

    public function getOccupation($id) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('Sky\SettingBundle\Entity\Occupation')->find($id);
    }

    public function getBibleClass($id) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('Sky\SettingBundle\Entity\BibleClass')->find($id);
    }

    //FUNCTION TO SAVE MESSAGE
    public function saveMessage($entity) {
        $em = $this->getDoctrine()->getManager();
        $em->merge($entity);
//        $em->persist($entity);
        $em->flush();
        $em->clear();
//        return true;
    }

    public function insertDevice($actorId, $value, $table) {

        $em = $this->getDoctrine()->getManager(); // ...or getEntityManager() prior to Symfony 2.1
        $connection = $em->getConnection();
        $statement = $connection->prepare("INSERT INTO $table (`actor_id`, `device_mechanism_id`) VALUES ($actorId, $value)");
//        $statement->bindValue('id', 1);
        $statement->execute();
    }

    public function myDeviceMech($param) {
        $deviceMechnismEntity = new DeviceMechanism();

        if (!empty(array_filter($param))) {
            $myDm = array();
            foreach ($param as $k => $v) {
                $val = trim($v);
                $value = preg_replace('!\s+!', ' ', $val);
                $nmtDm = $this->getDoctrine()->getRepository("SettingsBundle:DeviceMechanism")->findUnitByName(($value));
                if (!empty(($nmtDm))) {
                    $myDm[] = $nmtDm['id'];
                } else {
                    $deviceMechnismEntity->setName(ucfirst(strtolower($value)));
                    $deviceMechnismEntity->setCreatedAt(new \DateTime(date('Y-m-d H:i:s', time())));
                    $deviceMechnismEntity->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s', time())));
                    $this->saveMessage($deviceMechnismEntity);
                    $nmtDm = $this->getDoctrine()->getRepository("SettingsBundle:DeviceMechanism")->findUnitByName(($value));
                    $myDm[] = $nmtDm['id'];
                }
            }
            return $myDm;
        }
    }

    //FUNCTION TO SAVE MESSAGE
    public function saveMessage2($entity) {
        $em = $this->getDoctrine()->getManager();
//        $em->merge($entity);
        $em->persist($entity);
        $em->flush();

        return $entity->getId();

//        $em->clear();
//        return true;
    }

    //    //FUNCTION TO SAVE MESSAGE
//    public function saveMessage($entity) {
//        $em = $this->getDoctrine()->getManager();
//        $em->persist($entity);
//        $em->flush();
//        $em->clear();
////        return true;
//    }
}
