<?php

namespace Sky\ActorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ActorImportType extends AbstractType {

    private $user;
    private $er;

    public function __construct($user, $er) {
        $this->user = $user;
        $this->er = $er;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

//        $children = $this->getUserChildren($this->er, $this->user);
//        $parent = $this->user->getParent();
        $builder
               ->add('upload', 'file', array('required' => true, 'attr' => array('class' => 'form-control')))
//                ->add('actorGroup', null, array(
//                    'class' => 'MfarmActorBundle:ActorGroup',
//                    'error_bubbling' => true,
//                    'query_builder' => function(EntityRepository $er) use ($parent, $children) {
//
//                        $query = $er->createQueryBuilder('o')
//                                ->leftJoin('o.user', 'u')
//                                ->where('u.id = :user_id')
//                                ->andWhere('o.enable = :n')
//                                ->setParameter('n', 1)
//                                ->setParameter('user_id', $this->user->getId());
//                        if ($parent != null) {
//                            $query->Orwhere('u.id = :parent_id')
//                            ->setParameter('parent_id', $parent->getId());
//                        }
//                        if (!empty($children)) {
//                            $query->orWhere($query->expr()->in('o.user', $children));
//                        }
//                        return $query;
//                    }
//                        ), ['admin_code' => 'mfarm_actor.admin.actor']
//                )
//                ->add('country', null, array('required' => true))
//                ->add('region', 'shtumi_dependent_filtered_select2', array('required' => false, 'entity_alias' => 'region_by_country'
//                    , 'parent_field' => 'country', 'attr' => array('style' => 'width: 100%; ')), array('list' => 'list'))
//                
//                ->add('province', 'shtumi_dependent_filtered_select2', array('required' => false, 'entity_alias' => 'province_by_region'
//                    , 'parent_field' => 'region', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width: 100%; ')), array('list' => 'list'))
//                
//                ->add('district', 'shtumi_dependent_filtered_select2', array('required' => false, 'entity_alias' => 'district_by_region'
//                    , 'parent_field' => 'region', 'attr' => array('style' => 'width: 100%; ')), array('list' => 'list'))
//                ->add('town', 'shtumi_dependent_filtered_select2', array('required' => false, 'entity_alias' => 'town_by_district', 'empty_value' => 'Select Town'
//                    , 'parent_field' => 'district', 'attr' => array('style' => 'width: 100%; ')), array('list' => 'list'))
//                ->add('locality', 'shtumi_dependent_filtered_select2', array('required' => false, 'entity_alias' => 'locality_by_town'
//                    , 'parent_field' => 'town', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width:100%')), array('list' => 'list'))
//                
//                ->add('village', 'shtumi_dependent_filtered_select2', array('required' => false, 'entity_alias' => 'village_by_town', 'empty_value' => 'Select Village'
//                    , 'parent_field' => 'town', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width: 100%; ')), array('list' => 'list'))
//                
                ->add('user')
                ->add('search', 'submit', array('label' => 'Upload', 'attr' => array('class' => 'btn btn-success')))

        ;
    }

//    public function getUserChildren($er, $user) {
//        $info = $er->getChildren($user->getId());
//        return $info;
//    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sky\ActorBundle\Entity\Actor'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sky_actorbundle_actorimport';
    }

}
