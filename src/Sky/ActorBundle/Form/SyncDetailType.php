<?php

namespace Sjmcth\ActorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class SyncDetailType extends AbstractType {

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
        ->add('syncDate','datetime',array('required'=>true, 'widget' => 'single_text'))
        ->add('save','submit',array('attr'=>array('class'=>'btn btn-success btn-success-1')))
        ;
  }

  /**
   * @param OptionsResolverInterface $resolver
   */
  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
        'data_class' => 'Lg\SyncBundle\Entity\SyncDetail',
        'validation_groups' => array('default_syncDetail'),
        'csrf_protection' => false
    ));
  }

  /**
   * @return string
   */
  public function getName()
  {
      return '';
  }
}
