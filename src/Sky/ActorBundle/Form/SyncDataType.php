<?php

namespace Sjmcth\ActorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class SyncDataType extends AbstractType {

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
        ->add('name')
        ->add('sex')
        ->add('contact')
        ->add('save','submit',array('attr'=>array('class'=>'btn btn-success btn-success-1')))
        ;
  }

  /**
   * @param OptionsResolverInterface $resolver
   */
  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
        'csrf_protection' => false
    ));
  }

  /**
   * @return string
   */
  public function getName()
  {
      return 'form_name';
  }
}
