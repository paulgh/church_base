<?php

namespace Sky\ReportBundle\Handler;

use JMS\DiExtraBundle\Annotation as Di;
use Doctrine\Common\Persistence\ObjectManager;
use Sky\ApiBundle\Classes\Util;
use Symfony\Component\Form\FormFactoryInterface;
use Lg\ActorBundle\Form\SyncDetailType;
use Lg\ActorBundle\Form\SyncDataType;
use Doctrine\ORM\EntityManager;

/**
 * Campaign Handler 
 * @DI\Service("sql.api.handler")
 */
class SqlHandler {

    private $entityClass;
    private $repository;
    private $formFactory;
    private $context;
    private $connection;
    private $container;

//    private $surveyManager;

    /**
     * @DI\InjectParams({
     *     "om" = @DI\Inject("doctrine.orm.entity_manager"),
     * "formFactory" = @DI\Inject("form.factory"),
     *     "connection" = @DI\Inject("database_connection"),
     * "container" = @DI\Inject("service_container", required = false),
     *     "securityContext" = @DI\Inject("security.context", required = false),
     * "coreAdminManager" = @DI\Inject("core.admin.manager", required = false)
     * })
     */
    public function __construct(ObjectManager $om, FormFactoryInterface $formFactory, $connection, $container, $securityContext, $coreAdminManager) {
        $this->om = $om;
        $this->context = $securityContext;
        $this->syncDatas = array();
        $this->container = $container;
        $this->connection = $connection;
        $this->coreAdminManager = $coreAdminManager;
    }

    public function getSql($sql,$filters) {
        
        
       
        $parsed = $this->get_string_between($sql, '{', '}');
        $where=$wheres=$sqlWhere = NULL;
        if ('' != $parsed) {
            $wheres = explode(',', $parsed);
            $sqlString = preg_replace('/\{[^\]]+\}/', '[SQL]', $sql);
            
            foreach ($wheres as $where) {
                preg_match_all('/(?!\b)(:\w+\b)/', $where, $bindValues);
                $bindValue = str_replace(':', '', $bindValues[0][0]);
                if (isset($filters[$bindValue]) && '' != $filters[$bindValue]) {
                    if (null == $sqlWhere) {
                        $sqlWhere = ' where ';
                        $where = str_replace(array('and', 'or'), '', $where);
                    }
                    $sqlWhere .= $where;
                }
            }
            $sqlString = preg_replace('/\[[^\]]+\]/', $sqlWhere, $sqlString);
            $statement = $this->connection->prepare($sqlString);
            preg_match_all('/(?!\b)(:\w+\b)/', $sqlString, $bindVal);
            $bindVals = str_replace(':', '', $bindVal[0]);
            foreach ($bindVals as $bindVal) {
                $statement->bindValue($bindVal, $filters[$bindVal]);
            }
        } else {
            $statement = $this->connection->prepare($sql);
        }

        $statement->execute();
        $results = $statement->fetchAll();
        return $results;
    }

    function get_string_between($string, $start, $end) {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0)
            return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public function getColumns($dashId) {
        $data = $this->om->getRepository('ReportBundle:Columns')->findBy(array('dashboard' => $dashId));
        return $data;
    }

}
