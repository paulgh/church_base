<?php

namespace Sky\ReportBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\SecurityContext;

class DataFilterFormType extends AbstractType {
    //private $securityContext;
//    public function __construct(SecurityContext $securityContext) {
//        $this->securityContext = $securityContext;
//    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
//        $user = $this->securityContext->getToken()->getUser();

        $builder
                ->add('category', 'entity', array(
                    'empty_value' => 'Select category',
                    'class' => 'SettingsBundle:Category',
                    'required' => false,
                    'attr' => array('class' => '')
                ))
                ->add('movie', null, array(
                    'required' => false,
                    'attr' => array('placeholder' => 'Movie Title')
                        )
                )
                ->add('search', null, array(
                    'label' => 'Search',
                    'attr' => array('class' => 'btn btn-success'
                    )
                        )
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
//            'csrf_protection' => false
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return '';
    }

}
