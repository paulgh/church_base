<?php

namespace Sky\ReportBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sky\UserBundle\Entity\User;
use Sonata\AdminBundle\Route\RouteCollection;

class CategoryClassReportAdmin extends Admin {

    protected $baseRoutePattern = 'report/category';
    protected $baseRouteName = 'report_category';

    protected function configureRoutes(RouteCollection $collection) {
        $collection->clearExcept(array('list'));
    }

}
