<?php

namespace Sky\ReportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CoreController;
use Sonata\AdminBundle\Controller\CRUDController as cController;
use Sky\ReportBundle\Form\DataFilterFormType;
use Sky\FoodForAllBundle\Classes\Util;

//use mf\SettingsBundle\Entity\Crop;

class DashboardController extends controller {

    public function indexAction(Request $request = null) {

        dump('test');
        exit;

        $searchForm = $this->createForm(new DataFilterFormType());
        $searchForm
                ->add('submit', 'submit');

        $options = array(
            'agent' => $request->get('agent'),
            'customer' => $request->get('customer'),
            'from' => $request->get('from'),
            'to' => $request->get('to'),
        );


        $transaction2 = $this->getDoctrine()->getRepository("FoodForAllBundle:Transactions")->transactions($options);
        $transactionWeek = $this->getDoctrine()->getRepository("FoodForAllBundle:Transactions")->WeeklyTransactions($options);
        $transactionTwoWeeks = $this->getDoctrine()->getRepository("FoodForAllBundle:Transactions")->transactionTwoWeeks($options);
        $transactionMonth = $this->getDoctrine()->getRepository("FoodForAllBundle:Transactions")->transactionMonth($options);
        $transactionSixMonths = $this->getDoctrine()->getRepository("FoodForAllBundle:Transactions")->transactionSixMonths($options);
        $transactionYear = $this->getDoctrine()->getRepository("FoodForAllBundle:Transactions")->transactionYear($options);
        $transactionToday = $this->getDoctrine()->getRepository("FoodForAllBundle:Transactions")->transactionToday($options);
//        $handler = $this->container->get('basic.api.handler')->getInflow();



        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($transaction2, $page, $rows);
        $counter = Util::calculateRowCounterInitValue($rows, $page);
//       $pagination= $transaction2;

        return $this->render('ReportBundle:Dashboard:transactionReport.html.twig', array(
                    'pagination' => $pagination,
                    'transaction' => $transaction2,
                    'transactionWeek' => $transactionWeek,
                    'transactionTwoWeeks' => $transactionTwoWeeks,
                    'transactionMonth' => $transactionMonth,
                    'transactionSixMonths' => $transactionSixMonths,
                    'transactionYear' => $transactionYear,
                    'transactionToday' => $transactionToday,
                    'searchForm' => $searchForm->createView(),
                    'base_template' => $this->getBaseTemplate(),
                    'agent' => $request->get('agent'),
                    'customer' => $request->get('customer'),
                    'from' => $request->get('from'),
                    'to' => $request->get('to'),
                    'counter' => $counter,
                    'admin_pool' => $this->container->get('sonata.admin.pool'),
                    'blocks' => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks')
        ));
    }

    public function homeAction(Request $request = null) {

        $searchForm = $this->createForm(new DataFilterFormType());

        $category = $request->get('category');
        $movie = $request->get('movie');
        $options = array(
            'category' => $request->get('category'),
            'movie' => $request->get('movie'),
        );
        

        $allMovies = $this->getDoctrine()->getRepository("ActorBundle:Movie")->getAllMovies($options);
        $trending = $this->getDoctrine()->getRepository("ActorBundle:Movie")->getTrendingMovies($options);

        return $this->render('ReportBundle:Dashboard:homeReport.html.twig', array(
                    'category' => $category,
                    'movie' => $movie,
                    'allMovies' => $allMovies,
                    'trending' => $trending,
                    'searchForm' => $searchForm->createView(),
        ));
    }

    public function movieAction(Request $request = null) {
        $movie = $this->getDoctrine()->getRepository("ActorBundle:Movie")->getMovie($request->get('id'));
        $cinema = $this->getDoctrine()->getRepository("SettingsBundle:Showing")->findBy(Array('movie' => $request->get('id')));
        return $this->render('ReportBundle:Dashboard:movieDetail.html.twig', array(
                    'movie' => $movie,
                    'cinema' => $cinema,
        ));
    }
    public function movieCatAction(Request $request = null) {
        
        $options=null;
        $category = $this->getDoctrine()->getRepository("ActorBundle:Movie")->getAllMoviesCat($options);
        
        
        
        return $this->render('ReportBundle:Dashboard:category.html.twig', array(
                    'category' => $category,
        ));
    }

    public function paymentAction(Request $request = null) {
        return $this->render('ReportBundle:Dashboard:payment.html.twig', array(
        ));
    }

    public function aboutAction(Request $request = null) {
        return $this->render('ReportBundle:Dashboard:about.html.twig', array(
        ));
    }

}
