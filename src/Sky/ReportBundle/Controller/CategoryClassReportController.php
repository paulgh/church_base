<?php

namespace Sky\ReportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CoreController;

//use Sky\ReportBundle\Form\searchFormType;

class CategoryClassReportController extends CoreController {

    public function listAction(Request $request) {

        $options = null;
        $em = $this->getDoctrine()->getManager();
        //
////        $searchForm = $this->createForm(new searchFormType());
//
//        $from = $request->request->get('fromDate');
//        $to = $request->request->get('toDate');
//        $status = $request->request->get('membershipStatus');
//
//        $options = array(
//            'to' => $to,
//            'from' => $from,
//            'status' => $status
//        );

        $category = $em->getRepository('ActorBundle:Movie')->getAllMoviesCat($options);

        return $this->render('ReportBundle:Default:categoryClassReport.html.twig', array(
                    "category" => $category,
                    'admin_pool' => $this->container->get('sonata.admin.pool'),
                    'blocks' => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks')//                            )
                        )
                )
        ;
    }

}
