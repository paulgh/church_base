<?php

namespace Sky\SettingsBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * FarmerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DeviceMechanismRepository extends EntityRepository {
    public function findUnitByName($name = null) {
        $queryBuilder = $this->createQueryBuilder('u')
                ->select('u.id')
                ->where('u.name = :name')
                ->setParameter('name',$name)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        return $queryBuilder;
    }
}
