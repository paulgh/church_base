<?php

namespace Sky\SettingsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity(repositoryClass="\Sky\ActorBundle\Repository\ActorRepository")
 * @ORM\Table(name="showing")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")
 */
class Showing {

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="\Sky\SettingsBundle\Entity\Cinema")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cinema_id", referencedColumnName="id")
     * })
     */
    private $cinema;

    /**
     * @var 
     * @ORM\ManyToOne(targetEntity="\Sky\ActorBundle\Entity\Movie",inversedBy="actorMything",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="movie_id", referencedColumnName="id",nullable=true)
     * }))
     */
    private $movie;
    

    /**
     * @ORM\Column(name="showing_time",type="text",nullable=true)
     */
    private $showingTime;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return MyThing
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return MyThing
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Set showingTime
     *
     * @param string $showingTime
     * @return Showing
     */
    public function setShowingTime($showingTime) {
        $this->showingTime = $showingTime;

        return $this;
    }

    /**
     * Get showingTime
     *
     * @return string 
     */
    public function getShowingTime() {
        return $this->showingTime;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Showing
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    /**
     * Set cinema
     *
     * @param \Sky\SettingsBundle\Entity\Cinema $cinema
     * @return Showing
     */
    public function setCinema(\Sky\SettingsBundle\Entity\Cinema $cinema = null) {
        $this->cinema = $cinema;

        return $this;
    }

    /**
     * Get cinema
     *
     * @return \Sky\SettingsBundle\Entity\Cinema 
     */
    public function getCinema() {
        return $this->cinema;
    }


    /**
     * Set movie
     *
     * @param \Sky\ActorBundle\Entity\Movie $movie
     * @return Showing
     */
    public function setMovie(\Sky\ActorBundle\Entity\Movie $movie = null)
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * Get movie
     *
     * @return \Sky\ActorBundle\Entity\Movie 
     */
    public function getMovie()
    {
        return $this->movie;
    }
}
