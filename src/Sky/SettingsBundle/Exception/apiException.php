<?php

namespace Mfarm\SettingBundle\Exception;



class apiException extends \Exception {

  protected $form;
  protected $entity;

  public function __construct($message, $form = null, $entity = null)
  {
      parent::__construct($message);
  }

  /**
   * @return array|null
   */
  public function getForm()
  {
      if($this->entity)
        return array('form'=>$this->form,'entity'=>$this->entity);
      else
        return array('form'=>$this->form);
  }
}
