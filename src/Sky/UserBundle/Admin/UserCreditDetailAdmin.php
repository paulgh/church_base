<?php

namespace Sky\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sky\UserBundle\Entity\UserCreditDetail;

class UserCreditDetailAdmin extends Admin {

    /**
     * Create and Edit
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('credit')
                ->add('user',  null, array('required' => true, 'label' => 'User', 'query_builder' =>
                    function (\Doctrine\ORM\EntityRepository $repository) {
                        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
                        $user = $securityContext->getToken()->getUser();
                        if ($user->getUserName() != 'admin') {
                            return $repository->getAllrep($user);
                        } else {
                            return $repository->createQueryBuilder('u');
                        }
                    }))
                        
                        //null, array('expanded' => false, 'multiple' => false))
                ->end();
    }

    /**
     * List
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id')
                ->add('credit')
                ->add('user')
                ->add('createdAt')

        ;
    }

    /**
     * Filters
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('credit')
                ->add('user', null, array('required' => true, 'label' => 'User', 'query_builder' =>
                    function (\Doctrine\ORM\EntityRepository $repository) {
                        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
                        $user = $securityContext->getToken()->getUser();
                        if ($user->getUserName() != 'admin') {
                            return $repository->getAllrep($user);
                        } else {
                            return $repository->createQueryBuilder('u');
                        }
                    }))
                        
                        
                        //null, array('filter_field_options' => array('expanded' => true, 'multiple' => true)))

        ;
    }

    /**
     * Show
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper) {
        $showMapper
                ->add('id')
                ->add('user')

        ;
    }

    public function createQuery($context = 'list') {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        $user = $securityContext->getToken()->getUser();
        $query = parent::createQuery($context);
        if ($user->getUserName() != 'admin') {
//            UserFilter::filterByUser($query,$securityContext);
            $query = $this->getModelManager()->createQuery($this->getClass(), 'c');
            $query->leftJoin('c.user','u');
            $query->where('u.parent = :user_id')
                    ->setParameter('user_id', $user->getId());
        }

        return $query;
    }

}
