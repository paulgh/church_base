<?php

namespace Sky\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Doctrine\ORM\EntityRepository;

class UserAdmin extends Admin {

    /**
     * Create and Edit
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '500M');
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        $user = $securityContext->getToken()->getUser();


        $fileFieldOptions = array();
        if (null != $this->getSubject()) {
            $image = $this->getSubject()->getPix();

            // use $fileFieldOptions so we can add other options to the field
            $fileFieldOptions = array('required' => false);

            if ($image) {
                // get the container so the full path to the image can be set
                $container = $this->getConfigurationPool()->getContainer();
                $fullPath = $container->get('request')->getBasePath() . '/uploads/user/' . $image;

                // add a 'help' option containing the preview's img tag
                $fileFieldOptions['help'] = '<img id="editImage" width="100px" src="' . $fullPath . '" class="admin-preview" />';
            }
        }

        $formMapper
                ->tab('Personal Information')
                ->with('Personal Information', array(
                    'class' => 'col-md-12 success',
                    'box_class' => 'box box-solid box-success',
                ))
                ->add('imageFile', 'file', array(
                    'label' => 'Profile Picture',
                    'by_reference' => false,
                    'data_class' => null,
                    'required' => false), $fileFieldOptions)
                ->add('fullName', null, array("label" => 'Full Name',))
//                ->add('lastName', null, array("required" => true,))
//                ->add('gender', 'choice', array('choices' => array('Male' => 'Male', 'Female' => 'Female')))
//                ->add('dob', 'date', array(
//                    // render as a single text box
//                    'widget' => 'single_text',
//                    'required' => false
//                ))
                ->add('mobileNo', null, array("required" => false, 'help' => "Mobile Number should be with country code eg:<b>233xxxxxxxxx </b>"))
                ->add('email', null, array("required" => false, 'help' => " eg:<b> paulemgh@ecard.com </b>"))
//                ->add('branch', null, array(
//                    "required" => false,
//                ))
//                ->add('department', null, array(
//                    "required" => false,
//                ))
                ->end()
                ->end()
                ->tab('Login Details')
                ->with('Login Details', array(
                    'class' => 'col-md-12 success',
                    'box_class' => 'box box-solid box-success',
                ))
                ->add('username')
                ->add('plainpassword', null, array(
                    'label' => 'Plain Password',
                    'required' => true,
        ));

//                ->end()
//                ->end()
//                ->tab('Roles and Security')
//                ->with('Security', array(
//                    'class' => 'col-md-12 success',
//                    'box_class' => 'box box-solid box-success',
//                ))

        if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
            $formMapper
                    ->add('enabled')
                    ->add('groups', null, array(
                        'label' => 'User  Abilities and Roles',
                        'multiple' => true,
                        'required' => true
                    ))
                    ->add('userType', 'choice', array('choices' => array(
                            'Driver' => 'Driver',
                            'Agent' => 'Agent',
                            'Administrator' => 'Administrator',
                )))
            ;
        }


        $formMapper
                ->end()
                ->end()

        ;
    }

    /**
     * List
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id')
                ->add('pix', null, array(
                    'template' => 'UserBundle:Admin:list_pix_preview.html.twig',
                    'attr' => array('style' => 'width: 44.171%; ')))
                ->add('fullname')
                ->add('username')
                ->add('userType')
                ->add('mobileNo', null, array())
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * Filters
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('username')
                ->add('mobileNo')
//                ->add('gender')
                ->add('roles');
        ;
    }

    /**
     * Show
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper) {
        $showMapper
                ->add('pix', null, array(
                    'template' => 'UserBundle:Admin:list_pix_preview.html.twig',
                    'attr' => array('style' => 'width: 44.171%; ')))
                ->add('username')
//                ->add('gender')
                ->add('plainpassword', null, array('editable' => true, 'label' => 'Pin'))
//                ->add('userType', null, array('editable' => true))
//                ->add('mobileNo', null, array('editable' => true))
                ->add('groups')
//                ->add('phoneVerificationStatus', null, array('editable' => true))
//                ->add('registrationCompleteStatus', null, array('editable' => true))
//                ->add('pinSetStatus', null, array('editable' => true))
                ->add('fullName', null, array("label" => 'Full Name',))
//                ->add('lastName', null, array("required" => true,))
//                ->add('dob')



        ;
    }

//    public function createQuery($context = 'list') {
//        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
//        $user = $securityContext->getToken()->getUser();
//        $query = parent::createQuery($context);
//        if ($user->getUserName() != 'admin') {
////            UserFilter::filterByUser($query,$securityContext);
//            $query = $this->getModelManager()->createQuery($this->getClass(), 'u');
//            $query->where('u.parent = :user_id')
//                    ->setParameter('user_id', $user->getId());
//        }
//
//        return $query;
//    }

    public function getUser() {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        $user = $securityContext->getToken()->getUser();
        return $user;
    }

    public function prePersist($object) {
        $DM = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $password = $object->getPlainpassword();
        if (!empty($password)) {
            $salt = md5(time());

            $encoderservice = $this->getConfigurationPool()->getContainer()->get('security.encoder_factory');
            $encoder = $encoderservice->getEncoder($object);
            $encoded_pass = $encoder->encodePassword($object->getPassword(), $salt);
            $object->setSalt($salt);
            $object->setPassword($encoded_pass);
        }
    }

    public function preUpdate($object) {
        $DM = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $password = $object->getPlainpassword();
        if (!empty($password)) {
            $salt = md5(time());

            $encoderservice = $this->getConfigurationPool()->getContainer()->get('security.encoder_factory');
            $encoder = $encoderservice->getEncoder($object);
            $encoded_pass = $encoder->encodePassword($object->getPassword(), $salt);
            $object->setSalt($salt);
            $object->setPassword($encoded_pass);
        }
    }

}
