<?php

namespace Sky\UserBundle\Controller;

use Sky\UserBundle\Classes\RestController as Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use FOS\UserBundle\Model\UserInterface;
use Sky\UserBundle\Entity\User;
use Sky\UserBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sky\UserBundle\Entity\DeviceIdentity;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\HttpFoundation\JsonResponse;
use Ma27\ApiKeyAuthenticationBundle\Exception\CredentialException;
use Ma27\ApiKeyAuthenticationBundle\Model\User\ClassMetadata;
use Ma27\ApiKeyAuthenticationBundle\Security\ApiKeyAuthenticator;

class SecurityController extends Controller {

    /**
     *  @ApiDoc(
     *  resource=true,
     *  description="Log In User entity",
     *  parameters={
     * {"name"="_phone", "dataType"="string", "required"=false, "description"="username"},
     * {"name"="_password", "dataType"="string", "required"=true, "description"="password"},
     * {"name"="_deviceId", "dataType"="string", "required"=true, "description"="deviceId"},
     * 
     *  }
     * )
     * @Route("/login/api", name="api_login")
     * @return type
     */
    public function loginAction(Request $request) {
        $request = $this->getRequest();
        $session = $request->getSession();

        $username = $request->get("_phone");
        $password = $request->get("_password");
        $deviceid = $request->get("_deviceId");

//        dump($username);
//        exit;
        if (empty($username) AND empty($password)) {

            $username = $request->get("phone");
            $password = $request->get("password");
            $deviceid = $request->get("deviceId");
        }



        if (!empty($username) AND ! empty($password)) {

            $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

          
            if ($user != null AND is_object($user)) {

                $token = new UsernamePasswordToken($user, $password, "main", $user->getRoles());
                $passwordValid = $this->get("security.encoder_factory")->getEncoder($user)->isPasswordValid($user->getPassword(), $token->getCredentials(), $user->getSalt());

                if ($passwordValid) {
                    if (is_object($user)) {
                        $request = $this->get("request");
                        $event = new InteractiveLoginEvent($request, $token);
                        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

                        $apikey = bin2hex(openssl_random_pseudo_bytes(100));
                        $em = $this->getDoctrine()->getManager();
                        $device = $em->getRepository('UserBundle:DeviceIdentity')->findBy(array('device' => $deviceid));

                        
                     
                        if (!$device) {
                            $device = new DeviceIdentity();
                            $device->setToken($apikey);
                            $device->setUser($user);
                            $device->setDevice($deviceid);
                            $em->persist($device);
                            $em->flush();
                        } else {
                            $device = $device[0];
                            $device->setToken($apikey);
                            $em->persist($device);
                            $em->flush();
                        }

                        return $this->jsonResponse(array(
                                    'status' => 'true',
                                    'apiKey' => $apikey,
                                    'id' => $token->getUser()->getId(),
                                    'mobileNumber' => $token->getUser()->getMobileNo(),
                                    'phone_verification_status' => $token->getUser()->getPhoneVerificationStatus(),
                                    'first_name' => $token->getUser()->getFirstName(),
                                    'last_name' => $token->getUser()->getLastName(),
                                    'registration_complete_status' => $token->getUser()->getRegistrationCompleteStatus(),
                                    'pin_set_status' => $token->getUser()->getPinSetStatus(),
                                    'pixUrl' => $token->getUser()->getPixUrl(),
                        ));
                    }
                } else {
                    return $this->errorResponse("Bad credentials.", 401);
                }
            } else {
                return $this->errorResponse("No user found.");
            }
        }
    }

    /**
     * Log Out User entity
     *  @ApiDoc(
     *  resource=true,
     * )
     * @Route("/logout/api", name="api_logout")
     * @return type
     */
    public function logoutAction() {
        $user = $this->getUser();
        $devices = $user->getDeviceIdentity();
        foreach ($devices as $device) {
            $user->removeDeviceIdentity($device);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return new JsonResponse(array('status' => true, 'data' => array(
                'message' => "You have been logged out.",
            )
                )
        );
    }

    protected function getEngine() {

        return $this->container->getParameter('fos_user.template.engine');
    }

    /**
     *
     * @Route("/login/failure", name="login_failure")
     *
     * @return type
     */
    public function loginFailure() {

        return $this->errorResponse("Bad credentials", 401);
    }

    public function userProfileAction($id) {

        $user = $this->getDoctrine()->getRepository("APIBundle:User")->find($id);

        $profile = array();

        $profile["user"] = $user->restSerialize();
        $profile["canMakePayments"] = "false";
        $profile["pid"] = "";
        $profile["firstName"] = "";
        $profile["lastName"] = "";
        $profile["middleName"] = "";
        $profile["suffix"] = "";
        $profile["code"] = "";
        $profile["payment"] = "";
        $profile["dues"] = 0;
        $profile["joinDate"] = "";
        $profile["joinDate"]["date"] = new \DateTime();
        $profile["reason"] = "";
        $profile["comments"] = "";
        $profile["benefitDate"] = "";
        $profile["benefitDate"]["date"] = new \DateTime();
        $profile["contest"] = "XXXX";
        $profile["guide"] = "";
        $profile["email"] = "";
        $profile["cell"] = "";
        $profile["authorizeCustomerId"] = "";
        $profile["local"] = "";
        $profile["local"]["id"] = 1;
        $profile["local"]["name"] = "none";
        $profile["ssn"] = "";

        return $this->jsonResponse($profile);
    }

    public function profileAction($id) {

        $member = $this->getDoctrine()->getRepository("APIBundle:Member")->find($id);
        if (!is_object($member)) {
            $user = $this->getUser();
            $member = $user->getMember();
        } else {
            $user = $member->getUser();
        }



        if (!is_object($member)) {
            
        } else {
            $profile = $member->serialize();
            $profile["user"] = $user->restSerialize();
        }

        $paymentType = $member->getPaymentType();

        if (!is_object($paymentType)) {
            $paymentType = $this->getDoctrine()->getRepository("APIBundle:PaymentType")->find(2);
        }

        if ($this->canMakePayments($paymentType)) {
            $profile["canMakePayments"] = "true";
        } else {
            $profile["canMakePayments"] = "false";
        }

        $addresses = array();

        foreach ($member->getAddress() as $key => $value) {

            if (($value->getAddressType()) && ($value->getAddressType()->getName() == 'Shipping' || $value->getAddressType()->getName() == 'Billing')) {

                $address['city'] = $value->getCity();
                $address['street'] = $value->getStreet();
                $address['state'] = $value->getState();
                $address['zip'] = $value->getZip();
                $address['country'] = $value->getCountry();
                $addresses[$value->getAddressType()->getName()] = $address;
            }
        }
        $profile["address"] = $addresses;

        return $this->jsonResponse($profile);
    }

    private function canMakePayments(\API\Bundle\Entity\PaymentType $paymentType) {

        if ($paymentType->getId() == 8 OR $paymentType->getId() == 9) {
            return false;
        }

        return true;
    }

    /**
     * 
     * @Route("/user/register", name="user_register")
     * @Template("UserBundle:User:newUser.html.twig")
     * @return type
     */
    public function newUserAction() {
        $securityContext = $this->container->get('security.context');
        return $this->createForm(new UserType($securityContext));
    }

    /**
     * 
     * @Route("/user/create", name="post_user_register")
     * @Method({"POST"})
     * @return type
     */
    public function postUserAction(Request $request) {
        $user = new User();
        $em = $this->getDoctrine()->getManager();
        $factory = $this->get('security.encoder_factory');
        $securityContext = $this->container->get('security.context');

        $form = $this->createForm(new UserType($securityContext), $user);

        $form->bind($request);
        if ($form->isValid()) {

            $user = $form->getData();
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);
            $user->setUserType($this->get('userbundle.user.handler')->getAggregatorType());
            $user->setEnabled(true);
            $user->upload();
            $roles = array('ROLE_AGGREGATOR');
            $user->setRoles($roles);
            $em->persist($user);
            $em->flush($user);
            return $this->redirect($this->generateUrl('api_login_form'));
        }
        return $this->render('UserBundle:User:newUser.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}
