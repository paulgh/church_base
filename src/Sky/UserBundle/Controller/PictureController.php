<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Paul Gyamfi Fordjour | Ghana

namespace Sky\UserBundle\Controller;

use Sky\UserBundle\Form\UserType;
use Sky\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PictureController extends Controller {

    public function pictureUploadAction(Request $request = null) {
        try {

            $content = $this->getRequest()->request->all();
            $em = $this->getDoctrine()->getManager();
            if (!empty($content)) {

                $user = $em->getRepository("UserBundle:User")->find($content['user']);
                if (isset($content['imageName'])) {
                    $base64 = $this->convertToBase64($content['imageName']);
                    $photo = $this->setImage($base64);
//                    dump($photo);
//                    exit;
                    $user->setPix($photo);
                } else {
                    $response["success"] = 0;
                    $response["message"] = "Oops! image file not selected!!.";
                    $response = new Response(json_encode($response));
                    $response->setStatusCode(Response::HTTP_NOT_FOUND);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
                }
                $em->flush();
                return array('data' => "Profile Picture  updated successfully", 'status' => true);
//              
            } else {
                $response["success"] = 0;
                $response["message"] = "Oops! No parameters passed to post!!.";
                $response = new Response(json_encode($response));
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }

            return FOSView::create(array('errors' => "error occured"), Codes::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    protected function getUploadDir() {
//        $uploaddir = $_SERVER['DOCUMENT_ROOT'] . 'posreward/web/uploads/user/';
//        return $uploaddir;
        return $_SERVER['DOCUMENT_ROOT'] . '/uploads/user/';
    }

    // function to edit profile picture
    public function editProfilePictureAction(Request $request) {
        $userManager = $this->container->get('fos_user.user_manager');
        $results = $request->request->all();

//        dump($results);
//        exit;
        if ($results) {
            // get user by id
            $user = $userManager->findUserBy(array('id' => $results['user']));
            if ($user != null AND is_object($user)) {
//                dump($_FILES['imageName']);
//                exit;
                $file = $_FILES['imageName']['name'];
                if (isset($file)) {

                    $uploadfile = $_FILES['imageName']['tmp_name'];
//                    $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/geevapp/web/uploads/user/';
                    $uploaddir = $this->getUploadDir();

                    $oldPix = $user->getPix();

//                    dump($oldPix);
//                    exit;
                    $fileName = time() . "_" . $_FILES['imageName']['name'];
                    $uploadfile = $uploaddir . $fileName;

//                    dump($uploadfile);
//                    exit;
//                    dump($uploaddir);
//                    exit;
//                    chmod($uploaddir, 0777);
//                    $user->setImageName($fileName);
                    $user->setPix($fileName);
//                   
                    $move = move_uploaded_file($_FILES['imageName']['tmp_name'], $uploadfile);
                    if ($move) {
                        $d = $this->compress($uploadfile, $uploadfile, 90);
                        $userManager->updateUser($user);
                        $filePath = $this->getUploadDir() . $oldPix;
                        if (file_exists($filePath)) {
                            @unlink($filePath);
                        }
                        $response["success"] = 1;
                        $response["message"] = "Profile picture updated successfully.";
                        $response = new Response(json_encode($response));
                        $response->setStatusCode(Response::HTTP_OK);
                        $response->headers->set('Content-Type', 'application/json');
                        return $response;
                    } else {
                        $response["success"] = 0;
                        $response["message"] = "Image not saved!!.";
                        $response = new Response(json_encode($response));
                        $response->setStatusCode(Response::HTTP_NOT_FOUND);
                        $response->headers->set('Content-Type', 'application/json');
                        return $response;
                    }
                } else {
                    $response["success"] = 0;
                    $response["message"] = "No file selected!!.";
                    $response = new Response(json_encode($response));
                    $response->setStatusCode(Response::HTTP_NOT_FOUND);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
                }
            } else {
                // user not found
                $response["success"] = 0;
                $response["message"] = "Oops! User not found!!.";
                $response = new Response(json_encode($response));
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }
        } else {
            $response["success"] = 0;
            $response["message"] = "Oops! No parameters passed to post!!.";
            $response = new Response(json_encode($response));
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
//        dump($results);
//        exit;
    }

    // function to compress the image
    public function compress($source, $destination, $quality) {

        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source);
        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source);
        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source);

        imagejpeg($image, $destination, $quality);

        return $destination;
    }

    // function to convert to base64
    public function convertToBase64($path = null) {

        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = base64_encode($data);
        return $base64;
    }

    private function setImage($imgJson = null, $tableName = null) {

        $bytes = $imgJson;
//        dump($bytes);
//        exit;
        $fileName = \date('Y-m-d') . '_' . time() . '_' . ".jpeg";
        ;

        $filename = $this->createBytesArrayToImage($bytes, "uploads/user/", str_replace(' ', '_', $fileName));
        return $filename;
    }

    private function createBytesArrayToImage($byteArray, $path, $fileType = null) {
        $data = base64_decode($byteArray);
        if ($data != "") {
            $im = imagecreatefromstring($data);
            $filename = \date('Y-m-d') . '_' . time() . '_' . ".jpeg";

            if (!is_dir($path)) {
                mkdir($path, 0775, true);
            }

            //save the image to the disk
            if (isset($im) && $im != false) {
                $imgFile = $path . $filename;

                //delete the file if it already exists
                if (file_exists($imgFile)) {
                    unlink($imgFile);
                }
//                if ($fileType == 'image/jpeg') {
                $result = imagejpeg($im, $imgFile);
//                } else
//                if ($fileType == 'image/png') {
//                    $result = imagepng($im, $imgFile);
//                }
                imagedestroy($im);
                return $filename;
            } else {
                return false;
            }
        }
    }

}
