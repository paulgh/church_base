<?php

namespace Sky\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sky\UserBundle\Exception\InvalidFormException;
use Sky\UserBundle\Form\AgentType;
use Sky\UserBundle\Form\MapToReportType;
use Sky\UserBundle\Model\AgentInterface;
use APY\DataGridBundle\Grid\Source\Entity;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;

class AgentController extends FOSRestController {

  /**
   * List all agents.
   *
   * @ApiDoc(
   *   resource = true,
   *   statusCodes = {
   *     200 = "Returned when successful"
   *   }
   * )
   *
   * @Annotations\QueryParam(name="offset", requirements="datetime", nullable=true, description="Offset from which to start listing agents.")
   * @Annotations\QueryParam(name="limit", requirements="datetime", default="5", description="How many agents to return.")
   *
   * @Annotations\View(
   *  serializerEnableMaxDepthChecks=true
   * )
   *
   * @param Request               $request      the request object
   * @param ParamFetcherInterface $paramFetcher param fetcher service
   *
   * @return array
   * @Secure(roles="ROLE_AGGREGATOR, ROLE_ADMIN, ROLE_SUPER_ADMIN")
   */
  public function getAgentsAction(Request $request, ParamFetcherInterface $paramFetcher, $parentId)
  {
      $offset = $paramFetcher->get('offset');
      $offset = null == $offset ? 0 : $offset;
      $limit = $paramFetcher->get('limit');
      $em = $this->getDoctrine()->getManager();
      $paginator  = $this->get('knp_paginator');
      $user = $this->container->get('user.manager')->getCurrentUser();
      $parent = $em->getRepository('UserBundle:User')->find($parentId);
      if(is_object($parent) && $parentId == $user->getId() ){
          if($request->get('_format') == 'html' || $request->get('_format') == null){
                $query = $this->container->get('userbundle.agent.handler')->allDataQuery($parentId);
                $totalAgents = $this->container->get('userbundle.agent.handler')->countAllDataQuery($parentId);

              $pagination = $paginator->paginate(
                            $query,
                            $this->get('request')->query->get('page', 1)/*page number*/,
                            10/*limit per page*/
                          );
              return array('totalAgents'=>$totalAgents,'agents' => $pagination, 'parentId' => $parentId, 'mapdata' => array(), 'mapMoreData' => array(), 'counter' => 0 );
          }
          else{
              return $this->container->get('userbundle.agent.handler')->allAggentByUser($parent, $limit, $offset);
          }
      }else{
          throw new NotFoundHttpException(sprintf('The parent id  \'%s\' was not found.',$parentId));
      }
  }

  /**
   * Get single Agent.
   *
   * @ApiDoc(
   *   resource = true,
   *   description = "Gets a Agent for a given id",
   *   output = "Sky\UserBundle\Entity\Agent",
   *   statusCodes = {
   *     200 = "Returned when successful",
   *     404 = "Returned when the Agent is not found"
   *   }
   * )
   *
   * @Annotations\View(templateVar="agent",serializerEnableMaxDepthChecks=true)
   *
   * @param int     $id      the agent id
   *
   * @return array
   *
   * @throws NotFoundHttpException when agent not exist
   * @Secure(roles="ROLE_AGGREGATOR, ROLE_ADMIN, ROLE_SUPER_ADMIN")
   */
  public function getAgentAction($parentId, $id)
  {
      $em = $this->getDoctrine()->getManager();
      $parent = $em->getRepository('UserBundle:User')->find($parentId);
      if(is_object($parent)){
          $entity = $this->getOr404($parent, $id);
      }else{
          throw new NotFoundHttpException(sprintf('The parent id  \'%s\' was not found.',$parentId));
      }
  

      return $entity;
  }

  /**
   * Presents the form to use to create a new agent.
   *
   * @ApiDoc(
   *   resource = true,
   *   statusCodes = {
   *     200 = "Returned when successful"
   *   }
   * )
   *
   * @Annotations\View(
   *  templateVar = "form"
   * )
   *
   * @return FormTypeInterface
   * @Secure(roles="ROLE_AGGREGATOR, ROLE_ADMIN, ROLE_SUPER_ADMIN")
   */
  public function newAgentAction($parentId)
  {
      return array('form' => $this->createForm(new AgentType()), 'parentId' => $parentId);
  }

  /**
   * Create a Agent from the submitted data.
   *
   * @ApiDoc(
   *   resource = true,
   *   description = "Creates a new agent from the submitted data.",
   *   input = "Sky\UserBundle\Form\AgentType",
   *   statusCodes = {
   *     200 = "Returned when successful",
   *     400 = "Returned when the form has errors"
   *   }
   * )
   *
   * @Annotations\View(
   *  template = "UserBundle:Agent:newAgent.html.twig",
   *  statusCode = Codes::HTTP_BAD_REQUEST,
   *  templateVar = "form"
   * )
   *
   * @param Request $request the request object
   *
   * @return FormTypeInterface|View
   * @Secure(roles="ROLE_AGGREGATOR, ROLE_ADMIN, ROLE_SUPER_ADMIN")
   */
  public function postAgentAction(Request $request,$parentId)
  {
      $em = $this->getDoctrine()->getManager();
      $parent = $em->getRepository('UserBundle:User')->find($parentId);
      try {
      if(is_object($parent)){
        $form = $this->createForm(new AgentType());
        $form->bind($request);
        
        $logo =array('logo' => $form['logo']->getData() );
        $allParm = array_merge($request->request->all(),$logo);
        $newEntity = $this->container->get('userbundle.agent.handler')->post(
            $allParm,
            $parent
        );
      }else{
        throw new NotFoundHttpException(sprintf('The parent id  \'%s\' was not found.',$parentId));
      }

      $routeOptions = array(
          'id' => $newEntity->getId(),
          '_format' => $request->get('_format'),
          'parentId' => $parentId
      );
      if($request->get('_format') == 'html' || $request->get('_format') == null)
        return $this->routeRedirectView('api_userbundle_get_user_agents', $routeOptions, Codes::HTTP_CREATED);
      else
        return new Response('success');


      } catch (InvalidFormException $exception) {

          return $exception->getForm();
      }
  }

  /**
   * Update existing agent from the submitted data or create a new agent at a specific location.
   *
   * @ApiDoc(
   *   resource = true,
   *   input = "Sky\UserBundle\Form\AgentType",
   *   statusCodes = {
   *     201 = "Returned when the Agent is created",
   *     204 = "Returned when successful",
   *     400 = "Returned when the form has errors"
   *   }
   * )
   *
   * @Annotations\View(
   *  template = "UserBundle:Agent:editAgent.html.twig",
   *  templateVar = "form"
   * )
   *
   * @param Request $request the request object
   * @param int     $id      the agent id
   *
   * @return FormTypeInterface|View
   *
   * @throws NotFoundHttpException when agent not exist
   * @Secure(roles="ROLE_AGGREGATOR, ROLE_ADMIN, ROLE_SUPER_ADMIN")
   */
  public function putAgentAction(Request $request,$parentId, $id)
  {
  
      try {
                $em = $this->getDoctrine()->getManager();
                $parent = $em->getRepository('UserBundle:User')->find($parentId);
                $entity = $this->container->get('userbundle.agent.handler')->get($parent, $id);
            if($parent){
              if (!($entity)) {
                  $statusCode = Codes::HTTP_CREATED;
                  $form = $this->createForm(new AgentType());
                  $form->bind($request);
                  $logo =array('logo' => $form['logo']->getData() );
                  $allParm = array_merge($request->request->all(),$logo);
                  $newEntity = $this->container->get('userbundle.agent.handler')->post(
                      $allParm,
                      $parent
                  );
              } else {
                  $statusCode = Codes::HTTP_NO_CONTENT;
                  
                  $form = $this->createForm(new AgentType());
                  $form->bind($request);
                  $logo =array('logo' => $form['logo']->getData() );
                  $allParm = array_merge($request->request->all(),$logo);
                  $entity = $this->container->get('userbundle.agent.handler')->put(
                      $entity,
                      $allParm
                  );
              }
            }else{
              throw new NotFoundHttpException(sprintf('The parent id  \'%s\' was not found.',$parentId));
            }
            $routeOptions = array(
                'id' => $entity->getId(),
                '_format' => $request->get('_format'),
                'parentId' => $parentId
            );
            if($request->get('_format') == 'html' || $request->get('_format') == '')
              return $this->routeRedirectView('api_userbundle_get_user_agents',array('parentId'=>$parentId));
            else
              return new Response('success');

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
  }

  /**
   * Update existing agent from the submitted data or create a new agent at a specific location.
   *
   * @ApiDoc(
   *   resource = true,
   *   input = "Sky\UserBundle\Form\AgentType",
   *   statusCodes = {
   *     204 = "Returned when successful",
   *     400 = "Returned when the form has errors"
   *   }
   * )
   *
   * @Annotations\View(
   *  template = "UserBundle:Agent:editAgent.html.twig",
   *  templateVar = "form"
   * )
   *
   * @param Request $request the request object
   * @param int     $id      the agent id
   *
   * @return FormTypeInterface|View
   *
   * @throws NotFoundHttpException when agent not exist
   * @Secure(roles="ROLE_AGGREGATOR, ROLE_ADMIN, ROLE_SUPER_ADMIN")
   */
  public function patchAgentAction(Request $request, $parentId, $id)
  {
      $em = $this->getDoctrine()->getManager();
      $parent = $em->getRepository('UserBundle:User')->find($parentId);
      try {
            if(is_object($parent)  ){
                $entity = $this->container->get('userbundle.agent.handler')->patch(
                    $this->getOr404($id),
                    $request->request->all()
                );
            }else{
              throw new NotFoundHttpException(sprintf('The parent id  \'%s\' was not found.',$parentId));
            }

            $routeOptions = array(
                'id' => $entity->getId(),
                '_format' => $request->get('_format'),
                'parentId' => $parentId
            );
            if($request->get('_format') == 'html' || $request->get('_format') =='')
              return $this->routeRedirectView('api_userbundle_get_user_agent', $routeOptions, Codes::HTTP_NO_CONTENT);
            else
              return new Response('success');
        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
  }

  /**
   * Delete a <entiry> or throw an 404 Exception.
   *
   * @param mixed $ParentId
   * @param mixed $id
   *
   * @return AgentListView
   *
   * @throws NotFoundHttpException
   * @Secure(roles="ROLE_AGGREGATOR, ROLE_ADMIN, ROLE_SUPER_ADMIN")
   */
  public function deleteAgentAction(Request $request, $parentId, $id){
      $em = $this->getDoctrine()->getManager();
      $parent = $em->getRepository('UserBundle:User')->find($parentId);
      if(is_object($parent)){
        if(!$this->container->get('userbundle.agent.handler')->isSafeToDetele($id)){
          if($request->get('_format') == 'html' || $request->get('_format') ==''){
            $this->container->get('session')->getFlashBag()->set('error', 'This agent is in use, you can\'t delete it.');
            return $this->routeRedirectView('api_userbundle_get_user_agents',array('parentId'=>$parentId));
          }
        }
        if (!($entity = $this->container->get('userbundle.agent.handler')->delete($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }
      }else{
        throw new NotFoundHttpException(sprintf('The parent id  \'%s\' was not found.',$parentId));
      }

      if($request->get('_format') == 'html' || $request->get('_format') ==''){
        $this->container->get('session')->getFlashBag()->set('success', 'Agent deleted successfully.');
        return $this->routeRedirectView('api_userbundle_get_user_agents',array('parentId'=>$parentId));
      }
      else
        return new Response('success');
  }

  /**
   * Fetch a <entiry> or throw an 404 Exception.
   *
   * @param mixed $id
   *
   * @return AgentInterface
   *
   * @throws NotFoundHttpException
   * @Secure(roles="ROLE_AGGREGATOR, ROLE_ADMIN, ROLE_SUPER_ADMIN")
   */
  protected function getOr404($parent, $id)
  {
        if (!($entity = $this->container->get('userbundle.agent.handler')->get($parent,$id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }

        return $entity;
  }

  /**
   * Presents the form to use to edit agent.
   *
   * @ApiDoc(
   *   resource = true,
   *   statusCodes = {
   *     200 = "Returned when successful"
   *   }
   * )
   *
   * @Annotations\View(
   *  template = "UserBundle:Agent:editAgent.html.twig"
   * )
   *
   * @return FormTypeInterface
   * @Secure(roles="ROLE_AGGREGATOR, ROLE_ADMIN, ROLE_SUPER_ADMIN")
   */
  public function editAgentAction($parentId, $id)
  {
      $em = $this->getDoctrine()->getManager();
      $parent = $em->getRepository('UserBundle:User')->find($parentId);

      if(!is_object($parent)){
          throw new NotFoundHttpException(sprintf('The parent id  \'%s\' was not found.',$parentId));
      }

      if (!($entity = $this->container->get('userbundle.agent.handler')->get($parent,$id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
      }
      $form = $this->createForm(new AgentType(), $entity);
      return array('form'=>$form, 'entity' => $entity, 'parentId' => $parentId );
  }

  /**
   * Presents the form to use to edit agent.
   * @Annotations\View(
   *  template = "UserBundle:Agent:newMapToReport.html.twig",
   *  templateVar = "form"
   * )
   *
   * @return FormTypeInterface
   * @Secure(roles="ROLE_AGGREGATOR, ROLE_ADMIN, ROLE_SUPER_ADMIN")
   */
  public function newMaptoreportAction(){
      $user = $this->get('user.manager')->getCurrentUser();
      return array('form' => $this->createForm(new MapToReportType()), 'parentId' => $user->getId());
  }

  /**
   * Presents the form to use to edit agent.
   * @Annotations\View(
   *  template = "UserBundle:Agent:getMapToReports.html.twig",
   *  templateVar = "mapDatas"
   * )
   *
   * @return FormTypeInterface
   * @Secure(roles="ROLE_AGGREGATOR, ROLE_ADMIN, ROLE_SUPER_ADMIN")
   */
  public function postMaptoreportAction(){
    try{
        $user = $this->get('user.manager')->getCurrentUser();
        $paginator  = $this->get('knp_paginator');
        $request = $this->get('request');
        $form = $this->createForm(new MapToReportType());
        $form->bind($request);
        $options = array();
        if($form->isValid()){
          $data = $form->getData();
          $options['country'] = $data['country'];
          $options['region'] = $data['region'];
          $options['district'] = $data['district'];
          $options['userType'] = $data['userType'];
          $query = $this->container->get('userbundle.agent.handler')->getMapData($options);
          $pagination = $paginator->paginate(
                            $query,
                            $this->get('request')->query->get('page', 1)/*page number*/,
                            100000/*limit per page*/
                          );
          return array('mapDatas' => $pagination, 'userType' => ucfirst($options['userType']), 'mapdata' => array(), 'mapMoreData' => array(), 'counter' => 0, 'parentId' =>$user->getId() );
        }
        throw new InvalidFormException('Invalid submitted data', $form);
    }catch (InvalidFormException $exception) {

        return $exception->getForm();
    }
  }
}
