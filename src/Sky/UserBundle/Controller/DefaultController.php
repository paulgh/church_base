<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Paul Gyamfi Fordjour | Ghana

namespace Sky\UserBundle\Controller;

use Sky\UserBundle\Form\UserType;
use Sky\UserBundle\Entity\User;
use Sky\ActorBundle\Entity\Ticket;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class DefaultController extends Controller {

    // function to handle sms verification
    // function to handle account activation via sms
    /**
     *  @ApiDoc(
     *  resource=true,
     *  description="Log In User entity",
     *  parameters={
     * {"name"="smsVerificationCode", "dataType"="string", "required"=false, "description"="first name"},
     *
     * {"name"="phone", "dataType"="string", "required"=true, "description"="phone"}
     * 
     *  }
     * )
     * 
     * @return type
     */
    public function activateAccountBySmsAction(Request $request = null) {
        $user = new User();
        $userManager = $this->container->get('fos_user.user_manager');
        $results = $this->get('request')->request->all();
        if ($results) {

            $checksmsVerificationCode = $userManager->findUserBy(array('smsVerificationCode' => $results['smsVerificationCode'], 'mobileNo' => $results['phone']));
//            dump($checksmsVerificationCode);
//            exit;
            if ($checksmsVerificationCode) {
                if ($checksmsVerificationCode->getPhoneVerificationStatus() != null AND $checksmsVerificationCode->getPhoneVerificationStatus() != false) {
                    $response["success"] = 0;
                    $response["message"] = "Oops! Your account has already been activated!!.";
                    $response = new Response(json_encode($response));
                    $response->setStatusCode(Response::HTTP_OK);
                    $response->headers->set('Content-Type', 'application/json');

                    return $response;
                } else {
                    // activate the account and reset the verification code to null
                    $checksmsVerificationCode->setPhoneVerificationStatus(1);
                    $checksmsVerificationCode->setPinSetStatus(1);
                    $checksmsVerificationCode->setRegistrationCompleteStatus(1);
//                    $user->setSmsVerificationStatus(1);
                    $userManager->updateUser($checksmsVerificationCode);
                    $response["success"] = 1;
                    $response["message"] = "Activation successful.";
                    $response = new Response(json_encode($response));
                    $response->setStatusCode(Response::HTTP_OK);
                    $response->headers->set('Content-Type', 'application/json');

                    return $response;
                }
            } else {
                $response["success"] = 0;
                $response["message"] = "Oops! Wrong activation code!!.";
                $response = new Response(json_encode($response));
                $response->setStatusCode(Response::HTTP_OK);
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
        } else {
            $response["success"] = 0;
            $response["message"] = "Oops! No parameters passed to post!!.";
            $response = new Response(json_encode($response));
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    public function register2Action() {
        $user = new User();
        $userManager = $this->container->get('fos_user.user_manager');
        $factory = $this->get('security.encoder_factory');
        $securityContext = $this->container->get('security.context');

        $results = $this->get('request')->request->all();

        $em = $this->getDoctrine()->getManager();

//        dump($results);
//        exit;

        if ($results) {

            $checksmsVerificationCode = $userManager->findUserBy(array('id' => $results['id']));

//            dump($checksmsVerificationCode);
//            exit;

            if ($checksmsVerificationCode) {
                if ($checksmsVerificationCode->getRegistrationCompleteStatus() != null AND $checksmsVerificationCode->getRegistrationCompleteStatus() != false
                ) {
                    $response["success"] = 0;
                    $response["message"] = "Oops! Your account has already been completed!!.";
                    $response["user"] = $checksmsVerificationCode->getId();
                    $response = new Response(json_encode($response));
                    $response->setStatusCode(Response::HTTP_OK);
                    $response->headers->set('Content-Type', 'application/json');

                    return $response;
                } else {

                    $checksmsVerificationCode->setPlainpassword($results['pin']);
                    $encoder = $factory->getEncoder($checksmsVerificationCode);
                    $password = $encoder->encodePassword($checksmsVerificationCode->getPassword(), $checksmsVerificationCode->getSalt());
                    $checksmsVerificationCode->setPassword($password);

                    $country = $em->getRepository("LocationBundle:Country")->find($results['country']);
                    $country ? $checksmsVerificationCode->setCountry($country) : null;
                    // activate the account and reset the verification code to null
                    $checksmsVerificationCode->setPhoneVerificationStatus(1);

                    $checksmsVerificationCode->setBusinessAddress($results['businessAddress']);
                    $checksmsVerificationCode->setBusinessName($results['businessName']);
                    $checksmsVerificationCode->setPinSetStatus(1);
                    $checksmsVerificationCode->setRegistrationCompleteStatus(1);

//                    $user->setSmsVerificationStatus(1);
                    $userManager->updateUser($checksmsVerificationCode);
                    $response["success"] = 1;
                    $response["message"] = "Activation successful.";
                    $response = new Response(json_encode($response));
                    $response->setStatusCode(Response::HTTP_OK);
                    $response->headers->set('Content-Type', 'application/json');

                    return $response;
                }
            } else {
                $response["success"] = 0;
                $response["message"] = "Oops! Wrong activation code!!.";
                $response = new Response(json_encode($response));
                $response->setStatusCode(Response::HTTP_OK);
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
        } else {
            $response["success"] = 0;
            $response["message"] = "Oops! No parameters passed to post!!.";
            $response = new Response(json_encode($response));
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    // function to convert to base64
    public function convertToBase64($path = null) {

        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = base64_encode($data);
        return $base64;
    }

    private function setImage($imgJson = null, $tableName = null) {

        $bytes = $imgJson;
//        dump($bytes);
//        exit;
        $fileName = \date('Y-m-d') . '_' . time() . '_' . ".jpeg";
        ;

        $filename = $this->createBytesArrayToImage($bytes, "uploads/user/", str_replace(' ', '_', $fileName));
        return $filename;
    }

    private function createBytesArrayToImage($byteArray, $path, $fileType = null) {
        $data = base64_decode($byteArray);
        if ($data != "") {
            $im = imagecreatefromstring($data);
            $filename = \date('Y-m-d') . '_' . time() . '_' . ".jpeg";

            if (!is_dir($path)) {
                mkdir($path, 0775, true);
            }

            //save the image to the disk
            if (isset($im) && $im != false) {
                $imgFile = $path . $filename;

                //delete the file if it already exists
                if (file_exists($imgFile)) {
                    unlink($imgFile);
                }
//                if ($fileType == 'image/jpeg') {
                $result = imagejpeg($im, $imgFile);
//                } else
//                if ($fileType == 'image/png') {
//                    $result = imagepng($im, $imgFile);
//                }
                imagedestroy($im);
                return $filename;
            } else {
                return false;
            }
        }
    }

    /**
     *  @ApiDoc(
     *  resource=true,
     *  description="Log In User entity",
     *  parameters={
     * 
     * {"name"="phone", "dataType"="string", "required"=true, "description"="phone"},
     * {"name"="user", "dataType"="string", "required"=true, "description"="User Id"}
     * 
     *  }
     * )
     * 
     * @return type
     */
    public function resendsmsActivationCodeAction(Request $request = null) {
        $userManager = $this->container->get('fos_user.user_manager');
        $results = $this->get('request')->request->all();
        $em = $this->getDoctrine()->getManager();

//        dump($results);
//        $results['phone'];
//        exit;

        if ($results) {

            $checkPhoneNumber = $userManager->findUserBy(array('id' => $results['user']));

            if ($checkPhoneNumber) {
//                dump($checkPhoneNumber->getPhoneVerificationStatus());
//                exit;
                if ($checkPhoneNumber->getPhoneVerificationStatus() != null AND $checkPhoneNumber->getPhoneVerificationStatus() != false) {
                    $response["success"] = 1;
                    $response["message"] = "Oops! Your account has already been activated!!.";
                    $response = new Response(json_encode($response));
                    $response->setStatusCode(Response::HTTP_OK);
                    $response->headers->set('Content-Type', 'application/json');

                    return $response;
                } else {
                    // resent activation code
                    if ($results['phone'] != $checkPhoneNumber->getMobileNo()) {
                        // check if phone number exists for another user
                        $res = $em->getRepository("UserBundle:User")->findAnotherUserWithPhoneNumber($results['phone'], $results['user']);
                        if (empty($res)) {
                            $checkPhoneNumber->setMobileNo($results['phone']);
                            $checkPhoneNumber->setUsername($results['phone']);
                        } else {
                            $response["success"] = 0;
                            $response["message"] = "Another User With this phone number already exists.";
                            $response = new Response(json_encode($response));
                            $response->setStatusCode(Response::HTTP_OK);
                            $response->headers->set('Content-Type', 'application/json');

                            return $response;
                        }
//                        dump($res);
//                        exit;
                    }
                    $getSmsVerificationCode = $checkPhoneNumber->getSmsVerificationCode();
                    if (empty($getSmsVerificationCode)) {
                        $activationCode = $this->generateActivationCode();
                        $checkPhoneNumber->setSmsVerificationCode($activationCode);
//                    $user->setSmsVerificationStatus(1);
                        $userManager->updateUser($checkPhoneNumber);
                    } else {
                        $activationCode = $checkPhoneNumber->getSmsVerificationCode();
                    }


//                    dump($checkPhoneNumber->getSmsVerificationCode());
//                    dump($activationCode);
//                    exit;
                    // send sms with the activation code
                    // send verification code via email
                    // send sms with activation code
                    $message = "Hi " . $checkPhoneNumber->getFirstName() . ", your activation code is " . $activationCode;
                    $this->sendSms($results['phone'], "Food4All", $message);


                    $response["success"] = 1;
                    $response["message"] = "Activation code Successfully sent.";
                    $response = new Response(json_encode($response));
                    $response->setStatusCode(Response::HTTP_OK);
                    $response->headers->set('Content-Type', 'application/json');

                    return $response;
                }
            } else {
                $response["success"] = 0;
                $response["message"] = "Oops! User with this Phone number not registered on our platform!!Contact support if you registered with a different number.";
                $response = new Response(json_encode($response));
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
        } else {
            $response["success"] = 0;
            $response["message"] = "Oops! No parameters passed to post!!.";
            $response = new Response(json_encode($response));
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    // function to edit user profile

    /**
     *  @ApiDoc(
     *  resource=true,
     *  description="Log In User entity",
     *  parameters={
     * 
     * {"name"="firstName", "dataType"="string", "required"=true, "description"="First name"},
     * {"name"="lastName", "dataType"="string", "required"=true, "description"="Last Name"},
     * {"name"="businessAddress", "dataType"="string", "required"=true, "description"="Address"},
     * {"name"="dob", "dataType"="string", "required"=true, "description"="Date of Birth"},
     * {"name"="user", "dataType"="string", "required"=true, "description"="User Id"},
     * {"name"="gender", "dataType"="string", "required"=true, "description"="Sex"},
     * 
     *  }
     * )
     * 
     * @return type
     */
    public function editProfileAction(Request $request) {
        $userManager = $this->container->get('fos_user.user_manager');
        $results = $this->get('request')->request->all();
        $em = $this->getDoctrine()->getManager();

//        dump($results);
//        exit;

        if ($results) {
            $user = $userManager->findUserBy(array('id' => $results['user']));
            if ($user != null AND is_object($user)) {
//                dump($user);
                $user->setGender($results['gender']);
                $user->setFirstName($results['firstName']);
                $user->setLastName($results['lastName']);
                $user->setBusinessAddress($results['businessAddress']);
//                $user->setEmail($results['email']);
                $user->setDob(new \DateTime(date('Y-m-d', $this->processDate($results['dob']))));

                $userManager->updateUser($user);
                $response["success"] = 1;
                $response["message"] = "Profile updated successfully.";
                $response = new Response(json_encode($response));
                $response->setStatusCode(Response::HTTP_OK);
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            } else {
                // user not found
                $response["success"] = 0;
                $response["message"] = "Oops! User not found!!.";
                $response = new Response(json_encode($response));
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }
        } else {
            $response["success"] = 0;
            $response["message"] = "Oops! No parameters passed to post!!.";
            $response = new Response(json_encode($response));
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    // function to edit profile picture
    /**
     *  @ApiDoc(
     *  resource=true,
     *  description="Log In User entity",
     *  parameters={
     * 
     * {"name"="pix", "dataType"="file", "required"=true, "description"="Picture file"},
     * {"name"="user", "dataType"="string", "required"=true, "description"="User Id"},
     * 
     *  }
     * )
     * 
     * @return type
     */
    public function editProfilePictureAction(Request $request) {
        $userManager = $this->container->get('fos_user.user_manager');
        $results = $this->getRequest()->request->all();

        if ($results) {
            // get user by id
            $user = $userManager->findUserBy(array('id' => $results['user']));
            if ($user != null AND is_object($user)) {
//                dump($results);
//                exit;
                $file = $_FILES['pix']['name'];
                if (isset($file)) {

                    $uploadfile = $_FILES['pix']['tmp_name'];
                    $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/foodforall/web/uploads/user/';
//                    dump($uploaddir);
//                    exit;
//                    $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/user/';


                    $uploadfile = $uploaddir . basename(time() . "_" . $_FILES['pix']['name']);
                    $fileName = time() . "_" . $_FILES['pix']['name'];
//                    dump($uploaddir);
//                    exit;
//                    chmod($uploaddir, 0777);
                    $user->setPix($fileName);
//                   
                    $move = move_uploaded_file($_FILES['pix']['tmp_name'], $uploadfile);
                    if ($move) {
                        $userManager->updateUser($user);
                        $response["success"] = 1;
                        $response["message"] = "Profile picture updated successfully.";
                        $response = new Response(json_encode($response));
                        $response->setStatusCode(Response::HTTP_OK);
                        $response->headers->set('Content-Type', 'application/json');
                        return $response;
                    } else {
                        $response["success"] = 0;
                        $response["message"] = "Image not saved!!.";
                        $response = new Response(json_encode($response));
                        $response->setStatusCode(Response::HTTP_NOT_FOUND);
                        $response->headers->set('Content-Type', 'application/json');
                        return $response;
                    }
                } else {
                    $response["success"] = 0;
                    $response["message"] = "No file selected!!.";
                    $response = new Response(json_encode($response));
                    $response->setStatusCode(Response::HTTP_NOT_FOUND);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
                }
            } else {
                // user not found
                $response["success"] = 0;
                $response["message"] = "Oops! User not found!!.";
                $response = new Response(json_encode($response));
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }
        } else {
            $response["success"] = 0;
            $response["message"] = "Oops! No parameters passed to post!!.";
            $response = new Response(json_encode($response));
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
//        dump($results);
//        exit;
    }

    //function to do email verification
    public function emailVerificationAction(Request $request) {
        $userManager = $this->container->get('fos_user.user_manager');
        $results = $this->get('request')->request->all();

        if ($results) {

            $checkPhoneNumber = $userManager->findUserBy(array('email' => $results['email'], 'id' => $results['user']));
//            dump($checkPhoneNumber);
//            exit;


            if ($checkPhoneNumber) {
                if ($checkPhoneNumber->getEmailVerificationStatus() != null AND $checkPhoneNumber->getEmailVerificationStatus() != false) {
                    $response["success"] = 0;
                    $response["message"] = "Oops! Your email has already been verified!!.";
                    $response = new Response(json_encode($response));
                    $response->setStatusCode(Response::HTTP_NOT_FOUND);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
                } else {
                    // resent activation code
                    $activationCode = $this->generatePassword();
                    $checkPhoneNumber->setEmailVerificationCode($activationCode);

                    $userManager->updateUser($checkPhoneNumber);

                    // send email with activation code
//                    $this->sendEmailVerification("GeevApp Account Email Verification", $checkPhoneNumber->getFirstName(), $activationCode, "noreply@geevapp.com", $checkPhoneNumber->getEmail());
                    //$this->sendEmailVerification("GeevApp Account Email Verification", $checkPhoneNumber->getFirstName(), $activationCode, $checkPhoneNumber->getId(), "noreply@geevapp.com", $checkPhoneNumber->getEmail());

                    $firstName = $checkPhoneNumber->getFirstName();
                    $url = $this->generateUrl('verify_email', array('code' => $activationCode, 'id' => $checkPhoneNumber->getId()), UrlGeneratorInterface::ABSOLUTE_URL);

                    $em = $this->getDoctrine()->getManager();
                    $messages = $em->getRepository("SettingBundle:EmailTemplates")->findOneBy(array('title' => "Email Activation"));

                    $message = $messages ? $messages->getContent() : "Dear $firstName, <br/><br/>
                                                Thank you for opting to verify your email address.<br/>
                                                Click on this link to have your email verified.<br/><br/>
                                                <a href='$url'>Verify Email</a>
                                                <br/><br/>

                                                Thanks.<br/>
                                                Global Accelerex Limited POS Team.";


                    $message = str_replace("[Name]", $firstName, $message);
//                    $message = str_replace("[Activation Code]", $activationCode, $message);
                    $message = str_replace("[Title]", "GeevApp Account Email Verification", $message);
                    $message = str_replace("[Date]", date("Y-m-d", time()), $message);
                    $message = str_replace("[Year]", date("Y", time()), $message);
                    $message = str_replace("[url]", $url, $message);


//                    dump($message);
//                    exit;
                    $sendMail = $this->container->get('sendmail.api.handler');

                    $sendMail->sendEmail("GeevApp <support@gloabalaccelerex.com>", $checkPhoneNumber->getEmail(), "support@gloabalaccelerex.com", "GeevApp Account Email Verification", $message);
//                sendmail.api.handler

                    $response["success"] = 1;
                    $response["message"] = "Verification Email Successfully sent.";
                    $response = new Response(json_encode($response));
                    $response->setStatusCode(Response::HTTP_OK);
                    $response->headers->set('Content-Type', 'application/json');

                    return $response;
                }
//                dump($checkPhoneNumber->getSmsVerificationStatus());
//                exit;
            } else {
                $response["success"] = 0;
                $response["message"] = "Oops! User with this Email Address not registered on our platform!!Contact support if you registered with a different number.";
                $response = new Response(json_encode($response));
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
        } else {
            $response["success"] = 0;
            $response["message"] = "Oops! No parameters passed to post!!.";
            $response = new Response(json_encode($response));
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    // function to reset password
    public function forgotPasswordAction(Request $request) {
        $userManager = $this->container->get('fos_user.user_manager');
        $em = $this->getDoctrine()->getManager();
        $username = $request->request->get('phone');
//        $id = $request->request->get('user');
        $user = $em->getRepository("UserBundle:User")->findByMobileNo($username);
//        dump($user);
//        exit;
        $userObj = $user ? $user[0] : "";


        if ($username != "") {
            if ($user != null AND is_object($userObj)) {
                // now reset the password
                $newPassword = $this->generatePin();
                $userObj->setPassword($newPassword);
                $userObj->setPlainpassword($newPassword);

//                exit;

                $userManager->updateUser($userObj);

                $message = "Your temporal PIN is " . $newPassword;

                $this->sendSms($username, "POSReward", $message);
                $response["success"] = 1;
                $response["message"] = "Your PIN reset was  successful.We have sent you an sms with a temporal  PIN.Once you login with the temporal PIN, kindly reset to  a new PIN of your choice.";
                $response = new Response(json_encode($response));
                $response->setStatusCode(Response::HTTP_OK);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            } else {
                $response["error"] = 0;
                $response["message"] = "No user found";
                $response = new Response(json_encode($response));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }
        } else {
            $response["error"] = 0;
            $response["message"] = "Phone Number not passed to the api";
            $response = new Response(json_encode($response));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    public function resetPasswordAction(Request $request) {

        $oldPassword = $request->request->get('oldPassword');
        $newPassword = $request->request->get('newPassword');
        $confirmNewPassword = $request->request->get('confirmNewPassword');
        $userId = $request->request->get('user');

//        dump($oldPassword);
//        dump($newPassword);
//        dump($confirmNewPassword);
//        exit;

        $userManager = $this->container->get('fos_user.user_manager');
        $entity = $entity = $userManager->findUserBy(array('id' => $userId));
//        dump($entity);
//        exit;
        $user = new User();
        if ($oldPassword != "" && $newPassword != "" && $confirmNewPassword != "") {
            $encoderservice = $this->container->get('security.encoder_factory');
            $encoder = $encoderservice->getEncoder($user);
            $encoded_pass = $encoder->encodePassword($oldPassword, $entity->getSalt());

            if ($newPassword == $confirmNewPassword) {
                if ($entity->getPassword() == $encoded_pass) {
                    //now reset the password
//                    dump("good");
//                    exit;
                    $entity->setPassword($newPassword);
                    $entity->setPlainpassword($newPassword);
                    $userManager->updateUser($entity);
                    $response["success"] = 1;
                    $response["message"] = "Reset successful";
                    $response = new Response(json_encode($response));
                    $response->headers->set('Content-Type', 'application/json');

                    return $response;
                } else {
                    $response["error"] = 0;
                    $response["message"] = "Old Password is wrong";
                    $response = new Response(json_encode($response));
                    $response->headers->set('Content-Type', 'application/json');

                    return $response;
                }
            } else {
                $response["error"] = 0;
                $response["message"] = "New Password and Confirm New Password do not match";
                $response = new Response(json_encode($response));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }
        } else {
            $response["error"] = 0;
            $response["message"] = "All fields are required";
            $response = new Response(json_encode($response));
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }
    }

    // function to update profile picture
    public function updateProfilePixAction(Request $request) {

        $securityContext = $this->container->get('security.context');
        $user = new User();
        $form = $this->createForm(new UserType($securityContext));
        $form->handleRequest($request);


        $error = $form->getErrorsAsString();
        $factory = $this->get('security.encoder_factory');

        $userId = $securityContext->getToken()->getUser()->getId();

        if (@$_FILES['imageName']['tmp_name']) {
            $uploadfile = $_FILES['imageName']['tmp_name'];
            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/user/';
//            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/congress/web/uploads/user/';

            $uploadfile = $uploaddir . basename(time() . "_" . $_FILES['imageName']['name']);
            $fileName = time() . "_" . $_FILES['imageName']['name'];

//            print_r($uploaddir);
//            print_r($userId);
//            exit;

            if ($_FILES['imageName']['name']) {
                $user->setImageName($fileName);
                move_uploaded_file($_FILES['imageName']['tmp_name'], $uploadfile);

                // update the database
                $this->getDoctrine()->getRepository("UserBundle:User")->updateProfilePicture($fileName, $userId);
            }

            $this->getRequest()->getSession()->getFlashBag()->add("minagri_success", "Profile Picture Update successful");
            return $this->redirect($this->generateUrl('participant_edit_profile_picture'));
        }
        return $this->render('UserBundle:User:editProfilePicture.html.twig', array(
                    'form' => $this->createForm(new UserType($securityContext))->createView(),
        ));
    }

    // function to send sms  cocscon
    public function sendSms($phone, $sender_id = "Food4All", $message) {

        $apiKey = "4452d82e56f82a774dba85076bb6f8326fd3d5e3";
        $url = "http://bulk.myblastbox.com/api?key=" . $apiKey . "&phone=" . $phone . "&sender_id=" . $sender_id . "&message=" . urlencode($message);

        $sent = file_get_contents($url);
        if ($sent == "1604") {
            return true;
        } else {
            return false;
        }
    }

    public function send_mail($to, $subject, $msg, $headers) {
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($headers)
                ->setTo($to)
                ->setBody($msg);
        $this->get('mailer')->send($message);
    }

    // function to generate new password
    private function generatePassword() {
        return rand(111111111, 99999999);
    }

    // function to generate new password
    private function generateActivationCode() {
        return rand(1111, 9999);
    }

    // function to generate new password
    private function generatePin() {
        return rand(111111, 999999);
    }

    // function to send email
    private function sendEmail($subject = null, $name = null, $phone = null, $sender = null, $to) {
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($sender)
                ->setReplyTo($sender)
                ->setTo($to)
                ->setBody($this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/activation.html.twig', array('name' => $name, 'phone' => $phone)
                ), 'text/html');
        $sent = $this->get('mailer')->send($message);
        if ($sent) {
            return true;
        } else {
            return false;
        }
    }

    private function sendEmailVerification($subject = null, $name = null, $activationCode = null, $id = null, $sender = null, $to) {
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($sender)
                ->setReplyTo($sender)
                ->setTo($to)
                ->setBody($this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/email_verification.html.twig', array('name' => $name, 'code' => $activationCode, 'id' => $id)
                ), 'text/html');
        $sent = $this->get('mailer')->send($message);
        if ($sent) {
            return true;
        } else {
            return false;
        }
    }

    private function sendForgotPassword($subject = null, $name = null, $password = null, $sender = null, $to) {
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($sender)
                ->setReplyTo($sender)
                ->setTo($to)
                ->setBody($this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/forget_password_code.html.twig', array('name' => $name, 'password' => $password)
                ), 'text/html');
        $sent = $this->get('mailer')->send($message);
        if ($sent) {
            return true;
        } else {
            return false;
        }
    }

    private function sendForgotPasswordSimpleMail($subject = null, $name = null, $password = null, $sender = null, $to) {
        $headers = 'From: webmaster@example.com' . "\r\n" .
                'Reply-To: support@gloabalaccelerex.com' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        $message = "Dear $name, <br/><br/>
                Your new password is <b>$password</b>.<br/>
                Kindly use it to log in and change the password to suit what you can easily remember.
                <br/>
                Thanks.
                <br/>
                <br/>
                Global Accelerex Limited POS Team";
        mail($to, $subject, $message, $headers);
    }

    public function saveMessage($entity) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $em->clear();
//        return true;
    }

    public function loginAction(Request $request) {

        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('UserBundle:Default:login.html.twig', array(
                    'last_username' => $lastUsername,
                    'error' => $error,
        ));
    }

    public function forgetAction(Request $request) {
        $userForm = $this->createForm(new userType());
        $em = $this->getDoctrine()->getManager();
        $nameOrEmail = $request->request->get('username');
        $error = 1;
        $user = null;
        $pass = $this->generateRandomString();


        if ($request->isMethod('POST')) {
            $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($nameOrEmail);
            if ($user != null) {
                $data = $em->getRepository('UserBundle:User')->find($user);
                $email = $user->getEmail();
                $data->setPlainPassword($pass);
                $body = 'Your New Password is ' . $pass . ' login and change (Optional)';
                if ($email != null) {
                    $send = $this->sendEmail(' Password Reset', $body, $email, array('luutpal@gmail.com'), null, null, $heading = null, null);
                    $em->flush();
                    $error = null;
                } else {
                    $error = 3;
                }
            } else {
                $error = 2;
            }
        }
        return $this->render('UserBundle:Default:forget.html.twig', array(
                    'username' => $nameOrEmail,
                    'error' => $error,
                    'user' => $user,
                    'userForm' => $userForm->createView(),
                        )
        );
    }

    public function generateRandomString($length = 10) {

        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    private function processDate($date) {
        return strtotime($date);
    }

    public function registerAction1(Request $request = null) {


        $error = null;
        $tickNumber = $request->request->get('ticketNumber');
        $mobileNo = $request->request->get('mobileNo');

        $response = new Response(json_encode($response));


        if ($request->isXmlHttpRequest()) {

            $response->headers->set('Content-Type', 'application/json');
            $response["error"] = $tickNumber;

            return $response;
//            //check to ensure its only during token form
//            if ($userToken == null) {
//                //generate token and send to phone
//                $token = bin2hex(random_bytes(2));
//                //check if sms sent
//                $message = "Hello  Your token is " . $token;
//                $this->sendSms($mobileNo, "E-Ticket", $message);
//
//                $response["error"] = FALSE;
//                $response["message"] = ' Token sent to the mobile number provided. If you do not recieve sms contact Support on on Chat';
//
//                $response = new Response(json_encode($response));
//                $response->headers->set('Content-Type', 'application/json');
//                return $response;
//            } elseif ($userToken != '') {
//                return $response;
//            }
//            $response["error"] = True;
//            return $response;
        }
    }

    public function registerAction(Request $request = null) {
        $em = $this->getDoctrine()->getManager();

        $userForm = $this->createForm(new userType());
        $userToken = null;
        $ticket = new Ticket();
        $factory = $this->get('security.encoder_factory');
        $error = null;
        $ticketNumber = $request->request->get('ticketNumber');
        $finalCost = $request->request->get('finalCost');
        $movie = $request->request->get('movieName');
        $mobileNo = $request->request->get('mobileNo');
        $movieId = $request->request->get('movieId');
        $discount = $request->request->get('discount');

        $em = $this->getDoctrine()->getManager();

        if ($request->isXmlHttpRequest()) {
            $moveInstance = $this->getDoctrine()->getRepository('ActorBundle:Movie')->findOneBy(array('id' => $movieId));
            $token = bin2hex(random_bytes(2));


            $ticket->setAmountPaid($finalCost);
            $ticket->setDiscount($discount);
            $ticket->setRefCode($token);
            $ticket->setQuantity($ticketNumber);
            $ticket->setMobileNo($mobileNo);
            $ticket->setPurchaseSource('Web App');
            $ticket->setMovie($moveInstance);


//            $ticket->setMovie(1);
            //check to ensure its only during token form
            if ($userToken == null) {
                $this->saveMessage($ticket);
                //checks for uniques
                //generate token and send to phone
                //check if sms sent
                $message = "Hello, Pay an amount of " . $finalCost . " GHS with reference code " . $token . ' to our Merchant Numbers for ' . $ticketNumber . ' ticket(s) to ' . $movie;

                $this->sendSms($mobileNo, "MaesTec", $message);

                $response["error"] = FALSE;
                $response["message"] = ' Payment and reference information has  sent to the mobile number provided. If you do not recieve sms contact Support on Chat';
                $response["response"] = $message;
                $response = new Response(json_encode($response));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }
        } else {
            $response["error"] = TRUE;
        }
    }

    public function registerActionOld(Request $request = null) {
        $user = new User();
        $userManager = $this->container->get('fos_user.user_manager');

        $factory = $this->get('security.encoder_factory');
        $securityContext = $this->container->get('security.context');

        $results = $this->get('request')->request->all();

        $em = $this->getDoctrine()->getManager();
//        dump($results);
//        exit;

        if ($results) {
            $checkUserPhone = $userManager->findUserBy(array('mobileNo' => $results['phone']));

            if ($checkUserPhone) {
                // do an update
                $checkUserPhone->setEmail($results['phone']);
                $checkUserPhone->setFirstname($results['firstName']);
                $checkUserPhone->setLastname($results['lastName']);
                $checkUserPhone->setMobileNo($results['phone']);
                $checkUserPhone->setUsername($results['phone']);
                $checkUserPhone->setGender($results['gender']);
//                $date = strtotime($results['dob']);
//                $checkUserPhone->setDob(new \DateTime(date('Y-m-d', $date)));

                $userManager->updateUser($checkUserPhone);


                $response["success"] = 0;
                $response["message"] = "Oops! Mobile Number already exists!!.";
                $response["user"] = $checkUserPhone->getId(true);
                $response["phone_verification_status"] = $checkUserPhone->getPhoneVerificationStatus();
                $response["registration_complete_status"] = $checkUserPhone->getRegistrationCompleteStatus();
                $response["pin_set_status"] = $checkUserPhone->getPinSetStatus();
                $response = new Response(json_encode($response));
                $response->setStatusCode(Response::HTTP_OK);
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }

            $user->setEmail($results['phone']);
            $user->setFirstname($results['firstName']);
            $user->setLastname($results['lastName']);
            $user->setMobileNo($results['phone']);
            $user->setUsername($results['phone']);
            $user->setGender($results['gender']);
//            $date = strtotime($results['dob']);
//            $user->setDob(new \DateTime(date('Y-m-d', $date)));
//            $user->setBusinessAddress($results['businessAddress']);
//            $user->setBusinessName($results['businessName']);


            $activationCode = $this->generateActivationCode();
            $user->setPlainpassword($results['password']);
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);
            $user->setSmsVerificationCode($activationCode);
            $user->setPhoneVerificationStatus(0);
            $user->setPinSetStatus(0);
            $user->setRegistrationCompleteStatus(0);
            //// User name is same as mobile cos will be used to login
//            
//            $user->setRealRoles(array('ROLE_PARTICIPANT'));
            $user->setEnabled(true);
//            $user->addGroup(array);
            $this->saveMessage($user);



            // send sms with activation code
            $message = "Your verification code is " . $activationCode;
            $this->sendSms($results['phone'], "Food4All", $message);

            $response["success"] = 1;
            $response["message"] = "Activation code sent successfully.";
            $response["user"] = $user->getId(true);
            $response = new Response(json_encode($response));
            $response->setStatusCode(Response::HTTP_OK);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $response["success"] = 0;
            $response["message"] = "Oops! No parameters passed to post!!.";
            $response = new Response(json_encode($response));
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

//
//    public function sendSms($message = null, $customerNo = null) {
//
//        $ownerPhone = '0541363259';
//        $password = 'tayaako@1234';
//        $senderId = 'sjmcHarvest';
//
//        if ($customerNo != null && $message != '') {
//            $message = dump($message);
//            exit;
//            $url = "http://bulk.delsamobile.com/api?phone=" . $ownerPhone . "&password=" . $password . "&destination=" . $customerNo . "&sender_id=" . $senderId . "&message=" . urlencode($message);
//            $sent = file_get_contents($url);
//            if ($sent) {
//                return TRUE;
//            }
//        }
//    }

    public function smsCurlApi($url) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; CrawlBot/1.0.0)');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    # required for https urls
        curl_setopt($ch, CURLOPT_MAXREDIRS, 15);
        $html = curl_exec($ch);
        curl_close($ch);
        dump($html);
//        exit();
    }

}
