<?php

namespace Sky\UserBundle\Handler;

use Sky\UserBundle\Model\AgentInterface;
use Sky\UserBundle\Model\UserInterface;


interface AgentHandlerInterface {

  /**
   * Get a Agent given the identifier
   *
   * @api
   *
   * @param mixed $id
   *
   * @return AgentInterface
   */
  public function get(UserInterface $parent, $id);
  

  /**
   * Get a list of Agents.
   *
   * @param int $limit  the limit of the result
   * @param int $offset starting from the offset
   *
   * @return array
   */
  public function all(UserInterface $parent, $limit = 5, $offset = 0);

  /**
   * Post Agent, creates a new Agent.
   *
   * @api
   *
   * @param array $parameters
   *
   * @return AgentInterface
   */
  public function post(array $parameters, UserInterface $parent);

  /**
   * Edit a Agent.
   *
   * @api
   *
   * @param AgentInterface   $agent
   * @param array           $parameters
   *
   * @return AgentInterface
   */
  public function put(AgentInterface $agent, array $parameters);

  /**
   * Partially update a Agent.
   *
   * @api
   *
   * @param AgentInterface   $agent
   * @param array           $parameters
   *
   * @return AgentInterface
   */
  public function patch(AgentInterface $agent, array $parameters);
}
