<?php

namespace Sky\UserBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Sky\UserBundle\Entity\User as Agent;

/**
 * Agent Handler 
 * @DI\Service("user.manager")
 */
      
class UserManager {

  private $om;
  private $entityClass;
  private $repository;
  private $formFactory;
  private $context;

  /**
   * @DI\InjectParams({
   *     "om" = @DI\Inject("doctrine.orm.entity_manager"),
   *     "formFactory" = @DI\Inject("form.factory"),
   *     "securityContext" = @DI\Inject("security.context", required = false)
   * })
   */
  public function __construct(ObjectManager $om, FormFactoryInterface $formFactory, $securityContext)
  {
      $this->om = $om;
      $this->entityClass = "Sky\UserBundle\Entity\User";
      $this->repository = $this->om->getRepository($this->entityClass);
      $this->formFactory = $formFactory;
      $this->context = $securityContext;
  }

  /**
   * Get a logged User.
   *
   * @return Object $user
   */
  public function getCurrentUser()
  {
    return $this->context->getToken()->getUser();
  }

  /**
   * Get a logged User agent.
   *
   * @return Object $user
   */
  public function getAgentByUser($user = null )
  {
    if($user){
      return $this->repository->findByParent($user);
    }else{
      return $this->repository->findByParent($this->getCurrentUser());
    }
  }

  public function isValidFarmer($farmerId)
  {
      $user = $this->getCurrentUser();
      if($this->context->isGranted('ROLE_AGENT')){
          $user = $user->getParent();
      }
      if($farmer = $this->om->getRepository('FarmerBundle:Farmer')
                                    ->findBy(array('id'=>$farmerId, 'user' => $user))){
        return true;
      }else{
        return false;
      }

  }

  public function isValidFarmerGroup($farmerGroupId)
  {
      $user = $this->getCurrentUser();
      if($this->context->isGranted('ROLE_AGENT')){
          $user = $user->getParent();
      }
      if($farmer = $this->om->getRepository('FarmerBundle:FarmerGroup')
                                    ->findBy(array('id'=>$farmerGroupId, 'user' => $user))){
        return true;
      }else{
        return false;
      }

  }

  public function isValidContact($contactId)
  {
      $user = $this->getCurrentUser();
      if($this->context->isGranted('ROLE_AGENT')){
          $user = $user->getParent();
      }
      if($contact = $this->om->getRepository('ContactBundle:Contact')
                                    ->findBy(array('id'=>$contactId, 'user' => $user))){
        return true;
      }else{
        return false;
      }

  }

  public function isValidContactGroup($contactGroupId)
  {
      $user = $this->getCurrentUser();
      if($this->context->isGranted('ROLE_AGENT')){
          $user = $user->getParent();
      }
      if($contact = $this->om->getRepository('ContactBundle:ContactGroup')
                                    ->findBy(array('id'=>$contactGroupId, 'user' => $user))){
        return true;
      }else{
        return false;
      }

  }

  public function isValidAgent($agentId)
  {
      $user = $this->getCurrentUser();
      if($this->context->isGranted('ROLE_AGENT')){
          $user = $user->getParent();
      }
      if($user = $this->repository->findBy(array('id'=>$agentId, 'parent' => $user))){
        return true;
      }else{
        return false;
      }

  }

  public function isValidAssociation($associationId)
  {
      $user = $this->getCurrentUser();
      if($this->context->isGranted('ROLE_AGENT')){
          $user = $user->getParent();
      }
      if($farmer = $this->om->getRepository('AssociationBundle:Association')
                                    ->findBy(array('id'=>$associationId, 'user' => $user))){
        return true;
      }else{
        return false;
      }

  }

  public function isValidAssociationGroup($associationGroupId)
  {
      $user = $this->getCurrentUser();
      if($this->context->isGranted('ROLE_AGENT')){
          $user = $user->getParent();
      }
      if($farmer = $this->om->getRepository('AssociationBundle:AssociationGroup')
                                    ->findBy(array('id'=>$associationGroupId, 'user' => $user))){
        return true;
      }else{
        return false;
      }

  }

}
