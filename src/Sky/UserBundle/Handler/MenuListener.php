<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MenuListener
 *
 * @author root
 */
namespace Sky\UserBundle\Handler;
use Knp\Menu\MenuItem;

class MenuListener extends MenuItem {

    public function createMenu($event) {
        $menu = $event->getMenu();

        // create a new groupe
        $menu->addChild('System', array('translationDomain' => 'MyDomain'));

        // move user to System group
        $users = $menu->pop('sonata_user');
        $menu['System']->addChild($users);

        // add a divider to System group
        $menu['System']->addDivider();
        // ad a nav header
        $menu['System']->addNavHeader('Informations');

        // add About child
        $menu['System']->addChild('About', array('uri' => '#'));
        // add children to About
        $menu['System']['About']->addChild('Symfony', array('uri' => 'http://symfony.com'));
        $menu['System']['About']->addChild(
                'SonataAdminBundle', array(
            'uri' => 'http://sonata-project.org/bundles/admin/master/doc/index.html'
                )
        );
        $menu['System']['About']->addChild('VinceTAdminBundle', array('uri' => ''));
    }

}
