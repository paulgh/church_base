<?php

namespace Sky\UserBundle\Handler;

use JMS\DiExtraBundle\Annotation as Di;
use Doctrine\Common\Persistence\ObjectManager;
use Sky\ApiBundle\Classes\Util;
use Symfony\Component\Form\FormFactoryInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;

/**
 * Campaign Handler 
 * @DI\Service("sendmail.api.handler")
 */
class SendMailHandler extends EntityRepository {

    private $entityClass;
    private $repository;
    private $formFactory;
    private $context;
    private $connection;
    private $container;

//    private $surveyManager;

    /**
     * @DI\InjectParams({
     *     "om" = @DI\Inject("doctrine.orm.entity_manager"),
     * "formFactory" = @DI\Inject("form.factory"),
     *     "connection" = @DI\Inject("database_connection"),
     * "container" = @DI\Inject("service_container", required = false),
     *     "securityContext" = @DI\Inject("security.context", required = false),
     * "coreAdminManager" = @DI\Inject("core.admin.manager", required = false)
     * })
     */
    public function __construct(ObjectManager $om, FormFactoryInterface $formFactory, $connection, $container, $securityContext, $coreAdminManager) {
        $this->om = $om;
        $this->context = $securityContext;
//        $this->entityClass = "Sky\GeevappBundle\Entity\Actor";
        $this->repository = $this->om->getRepository("Sky\UserBundle\Entity\User");
        $this->syncDatas = array();
        $this->container = $container;
        $this->connection = $connection;
        $this->coreAdminManager = $coreAdminManager;
    }

    // function to send email
    public function sendEmail($from, $to, $replayTo = null, $subject = null, $message = null) {
        $headers = "From: $from" . "\r\n" .
                "Reply-To: $replayTo" . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        mail($to, $subject, $message, $headers);
    }

}
