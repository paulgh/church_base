<?php

namespace Sky\UserBundle\Classes;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializationContext;

/**
 * Description of RestController
 */
class RestController extends Controller {

  protected $mimeTypes = array(
    "json" => "application/json",
    "xml"  => "application/xml"
  );
  
  public function jmsSerialize($object) {
    
    $context = new SerializationContext();
    $context->setSerializeNull(true);
    $serializer = $this->container->get('jms_serializer');
    
    return json_decode($serializer->serialize($object, "json", $context));
    
  }
  
  public function messageResponse($message, $status = 200) {
    
    $response = new \stdClass();
    $response->message = $message;
    $response->status = "success";
    
    return new JsonResponse($response, $status);
    
  }
  
  public function jsonResponse($object, $status = 200) {
    
    return new JsonResponse($object, $status);
    
  }
  
  public function errorResponse($error, $status = 400) {
    
    $response = new \stdClass();
    $response->error = $error;
    
    return new JsonResponse($response, $status);
    
  }

  public function sendDeveloperData($object, $to) {

    ob_start();
    var_dump($object);
    $paramString = ob_get_clean();

    $message = \Swift_Message::newInstance()
      ->setSubject('API Bugs and Info')
      ->setFrom('no-reply@cleat.org')
      ->setTo($to)
      ->setBody($paramString);

    $this->container->get('mailer')->send($message);

  }

  public function sendMemberEmail($body, $to) {

    $message = \Swift_Message::newInstance()
      ->setSubject('CLEAT Tactical Bag')
      ->setFrom('no-reply@cleat.org')
      ->setTo($to)
      ->setBody($body);

    $this->container->get('mailer')->send($message);

  }

  public function determineProperResponse(Request $request, $data, $format) {

    $serializer = $this->get("jms_serializer");
    $responseData = $serializer->serialize($data, $format,SerializationContext::create()->enableMaxDepthChecks());

    return Response::create($responseData, 200, array(
      "Content-type" => $this->mimeTypes[$format]
    ));

  }

}