<?php
namespace Sky\UserBundle\Model;

interface AgentInterface
{
	/**
   * Get FirstName
   *
   * @return string 
   */
  public function getUsername();
  

  /**
   * Get FirstName
   *
   * @return string 
   */
  public function setUsername( $firstName);
}