<?php

namespace Sky\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Sky\UserBundle\Model\AgentInterface;
use Ma27\ApiKeyAuthenticationBundle\Annotation as Auth;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use \FOS\UserBundle\Entity\User as BaseUser;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
//use Cunningsoft\ChatBundle\Entity\AuthorInterface;
use JMS\Serializer\Annotation\Accessor;

/**
 * @Vich\Uploadable
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 * @ORM\Entity(repositoryClass="Sky\UserBundle\Repository\AgentRepository")
 * @ORM\Table(name="fos_user")
 * @UniqueEntity("username")
 * @UniqueEntity("mobileNo")
 * @ExclusionPolicy("all")
 */
class User extends BaseUser {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $groups;

    /**
     * 
     * @Assert\Length(
     *      min = 6,
     *      max = 243,
     *      minMessage = "Your Password must be at least {{ limit }} characters long",
     *      maxMessage = "Your Password cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(name="plain_password", type="string", nullable=true)
     *  Add a comment to this line
     */
    private $plainpassword;

    /**
     */
    protected $username;

    /** Get userType
     *
     * @return string
     */
    public function getUserType() {
        return $this->userType;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="user_type", type="string", length=255,nullable=true)
     */
    private $userType;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Sky\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @ORM\Column(name="full_name", type="string", length=255,nullable=true)
     * @Expose
     */
    private $fullName;

    /**
     * 
     * @Assert\Length(min= 10, max= 15)
     * @ORM\Column(name="mobile_no", type="string", length=20 , nullable=true)
     * @Expose
     */
    private $mobileNo;

    /**
     * @ORM\Column(name="pix", type="string", length=255, nullable=true)
     * @Expose
     */
    private $pix;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="user_image", fileNameProperty="pix")
     * 
     * @var File
     */
    private $imageFile;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Commodity
     */
    public function setImageFile(File $image = null) {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile() {
        return $this->imageFile;
    }

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        //$this->actor = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function setPlainpassword($password) {
        $this->plainpassword = $password;

        return $this;
    }

    public function getPlainpassword() {
        return $this->plainpassword;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return User
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    /**
     * Set pix
     *
     * @param string $pix
     * @return User
     */
    public function setPix($pix) {
        $this->pix = $pix;

        return $this;
    }

    /**
     * Get pix
     *
     * @return string 
     */
    public function getPix() {
        return $this->pix;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     * @return User
     */
    public function setFullName($fullName) {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getFullName() {
        return $this->fullName;
    }

    /**
     * Set mobileNo
     *
     * @param string $mobileNo
     * @return User
     */
    public function setMobileNo($mobileNo) {
        $this->mobileNo = $mobileNo;

        return $this;
    }

    /**
     * Get mobileNo
     *
     * @return string 
     */
    public function getMobileNo() {
        return $this->mobileNo;
    }

    /**
     * Set parent
     *
     * @param \Sky\UserBundle\Entity\User $parent
     * @return User
     */
    public function setParent(\Sky\UserBundle\Entity\User $parent = null) {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Sky\UserBundle\Entity\User 
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt() {
        return $this->salt;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     * @return User
     */
    public function setSalt($salt) {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set userType
     *
     * @param string $userType
     * @return User
     */
    public function setUserType($userType) {
        $this->userType = $userType;

        return $this;
    }

    /**
     * @Expose
     * @Accessor(getter="getPixUrl",setter="setPixUrl")
     */
    private $pixUrl;

    /**
     * Get userId
     *
     */
    public function getPixUrl() {
//        return $this->getUploadDir();
        return $this->pix ? $this->getUploadDir() . 'user/' . $this->pix : $this->getUploadDir() . "default/default_avatar.png";
    }

    protected function getUploadDir() {

        return 'http://' . $_SERVER['SERVER_NAME'] . '/uploads/';
    }

    public function __toString() {
        return $this->username;
    }

}
