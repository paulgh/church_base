<?php

namespace Sky\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DeviceIdentity
 *
 * @ORM\Table(name="device_identity")
 * @ORM\Entity(repositoryClass="Sky\UserBundle\Entity\DeviceIdentityRepository")
 */
class DeviceIdentity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="device", type="string", length=255)
     */
    private $device;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Sky\UserBundle\Entity\User",inversedBy="deviceIdentity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    public function __toString() {
        return $this->device;
        ;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return DeviceIdentity
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set device
     *
     * @param string $device
     * @return DeviceIdentity
     */
    public function setDevice($device)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return string 
     */
    public function getDevice()
    {
        return $this->device;
    }


    /**
     * Set user
     *
     * @param \Sky\UserBundle\Entity\User $user
     * @return DeviceIdentity
     */
    public function setUser(\Sky\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sky\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
