<?php

namespace Sky\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('createdAt')
            ->add('updatedAt')
            ->add('deletedAt')
            ->add('plainpassword')
            ->add('gender')
            ->add('fullName')
            ->add('middleName')
            ->add('lastName')
            ->add('mobileNo')
            ->add('dob')
            ->add('businessName')
            ->add('businessAddress')
            ->add('pix')
            ->add('phoneVerificationStatus')
            ->add('registrationCompleteStatus')
            ->add('pinSetStatus')
            ->add('smsVerificationCode')
            ->add('userType')
            ->add('parent')
            ->add('country')
            ->add('groups', null, array(
            'label' => 'User  Abilities and Roles',
            'multiple' => FALSE,
            'required' => true
            ));
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sky\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Sky_userbundle_user';
    }
}
