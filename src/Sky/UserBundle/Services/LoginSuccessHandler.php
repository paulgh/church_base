<?php
namespace Sky\UserBundle\Services;
 
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
 
class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    
    protected $router;
    protected $security;
    
    public function __construct(Router $router, SecurityContext $security)
    {
        $this->router = $router;
        $this->security = $security;
    }
    
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $response = new RedirectResponse($this->router->generate('sonata_admin_dashboard'));
        
//        if ($this->security->isGranted('ROLE_SUPER_ADMIN'))
//        {
//            $response = new RedirectResponse($this->router->generate('sonata_admin_dashboard')); 
//
//        }
//        elseif ($this->security->isGranted('ROLE_ADMIN'))
//        {
//            $response = new RedirectResponse($this->router->generate('sonata_admin_dashboard'));
//        } 
//        elseif ($this->security->isGranted('ROLE_USER'))
//        {
//            // redirect the user to where they were before the login process begun.
//            $referer_url = $this->router->generate('sonata_admin_dashboard');
//                        
//            $response = new RedirectResponse($referer_url);
//        }
        
        return $response;
    }
    
}