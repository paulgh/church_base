<?php

namespace Sky\LocationBundle\Model;



interface CountryInterface {

  /**
   * Get Name
   *
   * @return string 
   */
  public function getName();
  

  /**
   * Get Name
   *
   * @return string 
   */
  public function setName( $name);
  

  /**
   * Get CreatedAt
   *
   * @return string 
   */
  public function getCreatedAt();
  

  /**
   * Get CreatedAt
   *
   * @return string 
   */
  public function setCreatedAt( $createdAt);
  

  /**
   * Get UpdatedAt
   *
   * @return string 
   */
  public function getUpdatedAt();
  

  /**
   * Get UpdatedAt
   *
   * @return string 
   */
  public function setUpdatedAt( $updatedAt);
  

  /**
   * Add region
   *
   * @param Sky\LocationBundle\Entity\Region $region
   * @return Country
   */
  public function addRegion(\Sky\LocationBundle\Entity\Region $region);
  

  /**
   * Remove region
   *
   * @param Sky\LocationBundle\Entity\Region $region
   */
  public function removeRegion(\Sky\LocationBundle\Entity\Region $region);
  

  /**
   * Get Region
   *
   * @return \Doctrine\Common\Collections\Collection 
   */
  public function getRegion();
  
}
