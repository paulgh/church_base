<?php

namespace Sky\LocationBundle\Model;



interface DistrictInterface {

  /**
   * Get Name
   *
   * @return string 
   */
  public function getName();
  

  /**
   * Get Name
   *
   * @return string 
   */
  public function setName( $name);
  

  /**
   * Get CreatedAt
   *
   * @return string 
   */
  public function getCreatedAt();
  

  /**
   * Get CreatedAt
   *
   * @return string 
   */
  public function setCreatedAt( $createdAt);
  

  /**
   * Get UpdatedAt
   *
   * @return string 
   */
  public function getUpdatedAt();
  

  /**
   * Get UpdatedAt
   *
   * @return string 
   */
  public function setUpdatedAt( $updatedAt);
  

  /**
   * Get Country
   *
   * @return string 
   */
  public function getCountry();
  

  /**
   * Get Country
   *
   * @return string 
   */
  public function setCountry(\Sky\LocationBundle\Entity\Country $country);
  

  /**
   * Get Region
   *
   * @return string 
   */
  public function getRegion();
  

  /**
   * Get Region
   *
   * @return string 
   */
  public function setRegion(\Sky\LocationBundle\Entity\Region $region);
  

  /**
   * Add towns
   *
   * @param Sky\LocationBundle\Entity\Town $towns
   * @return District
   */
  public function addTown(\Sky\LocationBundle\Entity\Town $towns);
  

  /**
   * Remove towns
   *
   * @param Sky\LocationBundle\Entity\Town $towns
   */
  public function removeTown(\Sky\LocationBundle\Entity\Town $towns);
  

  /**
   * Get Towns
   *
   * @return \Doctrine\Common\Collections\Collection 
   */
  public function getTowns();
  
}
