<?php

namespace Sky\LocationBundle\Model;



interface TownInterface {

  /**
   * Get Name
   *
   * @return string 
   */
  public function getName();
  

  /**
   * Get Name
   *
   * @return string 
   */
  public function setName( $name);
  

  /**
   * Get CreatedAt
   *
   * @return string 
   */
  public function getCreatedAt();
  

  /**
   * Get CreatedAt
   *
   * @return string 
   */
  public function setCreatedAt( $createdAt);
  

  /**
   * Get UpdatedAt
   *
   * @return string 
   */
  public function getUpdatedAt();
  

  /**
   * Get UpdatedAt
   *
   * @return string 
   */
  public function setUpdatedAt( $updatedAt);
  

  /**
   * Get Country
   *
   * @return string 
   */
  public function getCountry();
  

  /**
   * Get Country
   *
   * @return string 
   */
  public function setCountry(\Sky\LocationBundle\Entity\Country $country);
  

  /**
   * Get Region
   *
   * @return string 
   */
  public function getRegion();
  

  /**
   * Get Region
   *
   * @return string 
   */
  public function setRegion(\Sky\LocationBundle\Entity\Region $region);
  

  /**
   * Get District
   *
   * @return string 
   */
  public function getDistrict();
  

  /**
   * Get District
   *
   * @return string 
   */
  public function setDistrict(\Sky\LocationBundle\Entity\District $district);
  
}
