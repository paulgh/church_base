<?php

namespace Sky\LocationBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Sky\LocationBundle\Model\CountryInterface;
use Sky\LocationBundle\Form\CountryType;
use Sky\LocationBundle\Exception\InvalidFormException;


/**
 * Country Handler 
 * @DI\Service("locationbundle.country.handler")
 */
      
class CountryHandler implements CountryHandlerInterface {

  private $om;
  private $entityClass;
  private $repository;
  private $formFactory;

  /**
   * @DI\InjectParams({
   *     "om" = @DI\Inject("doctrine.orm.entity_manager"),
   *     "formFactory" = @DI\Inject("form.factory"),
   * })
   */
  public function __construct(ObjectManager $om, FormFactoryInterface $formFactory)
  {
      $this->om = $om;
      $this->entityClass = "Sky\LocationBundle\Entity\Country";
      $this->repository = $this->om->getRepository($this->entityClass);
      $this->formFactory = $formFactory;
  }

  /**
   * Get a Country.
   *
   * @param mixed $id
   *
   * @return CountryInterface
   */
  public function get($id)
  {
      return $this->repository->find($id);
  }

  /**
   * Get a list of Countrys.
   *
   * @param int $limit  the limit of the result
   * @param int $offset starting from the offset
   *
   * @return array
   */
  public function all($limit = 5, $offset = 0)
  {
      return $this->repository->findAll();
  }
  
  /**
   * Get a list of Farmers.
   *
   * @return array
   */
  public function allDataQuery()
  {
      return $this->repository->getAll();
  }

  /**
   * Create a new Country.
   *
   * @param array $parameters
   *
   * @return CountryInterface
   */
  public function post(array $parameters)
  {
      $country = $this->createCountry();

      return $this->processForm($country, $parameters, 'POST');
  }

  /**
   * Edit a Country.
   *
   * @param CountryInterface $country
   * @param array         $parameters
   *
   * @return CountryInterface
   */
  public function put(CountryInterface $country, array $parameters)
  {
    return $this->processForm($country, $parameters, 'PUT');
  }

  /**
   * Partially update a Country.
   *
   * @param CountryInterface $country
   * @param array         $parameters
   *
   * @return CountryInterface
   */
  public function patch(CountryInterface $country, array $parameters)
  {
      return $this->processForm($country, $parameters, 'PATCH');
  }

  /**
   * Delete a Country.
   *
   * @return boolean
   */
  public function delete($id)
  {
      $entity = $this->repository->find($id);
      if(is_object($entity)){
        $this->om->remove($entity);
        $this->om->flush();
        return true;
      }else{
        return false;
      }
  }

  /**
   * Processes the form.
   *
   * @param CountryInterface $country
   * @param array         $parameters
   * @param String        $method
   *
   * @return CountryInterface
   *
   * @throws Sky\LocationBundle\Exception\InvalidFormException
   */
  private function processForm(CountryInterface $country, array $parameters, $method = "PUT")
  {
      $form = $this->formFactory->create(new CountryType(), $country, array('method' => $method));
      if(array_key_exists('_method', $parameters))
        unset($parameters['_method']);
      $form->submit($parameters, 'PATCH' !== $method);
      if ($form->isValid()) {

          $country = $form->getData();
          $this->om->persist($country);
          $this->om->flush($country);

          return $country;
      }

      throw new InvalidFormException('Invalid submitted data', $form, $country);
  }

  private function createCountry()
  {
      return new $this->entityClass();
  }

  public function getCountryCode(){
        return $this->createCountry()->getCountryCodeForUser();
  }
  public function getCurrency(){
        return $this->createCountry()->getCurrency();
  }
  public function getDataForSync(){
    if(array_key_exists('softdeleteable',$this->om->getFilters()->getEnabledFilters())){
        $this->om->getFilters()->disable('softdeleteable');
    }
    return$this->repository->getDataForSync();
  }
}
