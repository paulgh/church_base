<?php

namespace Sky\LocationBundle\Handler;

use Sky\LocationBundle\Model\DistrictInterface;


interface DistrictHandlerInterface {

  /**
   * Get a District given the identifier
   *
   * @api
   *
   * @param mixed $id
   *
   * @return DistrictInterface
   */
  public function get($id);
  

  /**
   * Get a list of Districts.
   *
   * @param int $limit  the limit of the result
   * @param int $offset starting from the offset
   *
   * @return array
   */
  public function all($limit = 5, $offset = 0);

  /**
   * Post District, creates a new District.
   *
   * @api
   *
   * @param array $parameters
   *
   * @return DistrictInterface
   */
  public function post(array $parameters);

  /**
   * Edit a District.
   *
   * @api
   *
   * @param DistrictInterface   $district
   * @param array           $parameters
   *
   * @return DistrictInterface
   */
  public function put(DistrictInterface $district, array $parameters);

  /**
   * Partially update a District.
   *
   * @api
   *
   * @param DistrictInterface   $district
   * @param array           $parameters
   *
   * @return DistrictInterface
   */
  public function patch(DistrictInterface $district, array $parameters);
}
