<?php

namespace Sky\LocationBundle\Handler;

use Sky\LocationBundle\Model\RegionInterface;


interface RegionHandlerInterface {

  /**
   * Get a Region given the identifier
   *
   * @api
   *
   * @param mixed $id
   *
   * @return RegionInterface
   */
  public function get($id);
  

  /**
   * Get a list of Regions.
   *
   * @param int $limit  the limit of the result
   * @param int $offset starting from the offset
   *
   * @return array
   */
  public function all($limit = 5, $offset = 0);

  /**
   * Post Region, creates a new Region.
   *
   * @api
   *
   * @param array $parameters
   *
   * @return RegionInterface
   */
  public function post(array $parameters);

  /**
   * Edit a Region.
   *
   * @api
   *
   * @param RegionInterface   $region
   * @param array           $parameters
   *
   * @return RegionInterface
   */
  public function put(RegionInterface $region, array $parameters);

  /**
   * Partially update a Region.
   *
   * @api
   *
   * @param RegionInterface   $region
   * @param array           $parameters
   *
   * @return RegionInterface
   */
  public function patch(RegionInterface $region, array $parameters);
}
