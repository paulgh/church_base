<?php

namespace Sky\LocationBundle\Handler;

use Sky\LocationBundle\Model\CountryInterface;


interface CountryHandlerInterface {

  /**
   * Get a Country given the identifier
   *
   * @api
   *
   * @param mixed $id
   *
   * @return CountryInterface
   */
  public function get($id);
  

  /**
   * Get a list of Countrys.
   *
   * @param int $limit  the limit of the result
   * @param int $offset starting from the offset
   *
   * @return array
   */
  public function all($limit = 5, $offset = 0);

  /**
   * Post Country, creates a new Country.
   *
   * @api
   *
   * @param array $parameters
   *
   * @return CountryInterface
   */
  public function post(array $parameters);

  /**
   * Edit a Country.
   *
   * @api
   *
   * @param CountryInterface   $country
   * @param array           $parameters
   *
   * @return CountryInterface
   */
  public function put(CountryInterface $country, array $parameters);

  /**
   * Partially update a Country.
   *
   * @api
   *
   * @param CountryInterface   $country
   * @param array           $parameters
   *
   * @return CountryInterface
   */
  public function patch(CountryInterface $country, array $parameters);
}
