<?php

namespace Sky\LocationBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Sky\LocationBundle\Model\RegionInterface;
use Sky\LocationBundle\Form\RegionType;
use Sky\LocationBundle\Exception\InvalidFormException;


/**
 * Region Handler 
 * @DI\Service("locationbundle.region.handler")
 */
      
class RegionHandler implements RegionHandlerInterface {

  private $om;
  private $entityClass;
  private $repository;
  private $formFactory;

  /**
   * @DI\InjectParams({
   *     "om" = @DI\Inject("doctrine.orm.entity_manager"),
   *     "formFactory" = @DI\Inject("form.factory"),
   * })
   */
  public function __construct(ObjectManager $om, FormFactoryInterface $formFactory)
  {
      $this->om = $om;
      $this->entityClass = "Sky\LocationBundle\Entity\Region";
      $this->repository = $this->om->getRepository($this->entityClass);
      $this->formFactory = $formFactory;
  }

  /**
   * Get a Region.
   *
   * @param mixed $id
   *
   * @return RegionInterface
   */
  public function get($id)
  {
      return $this->repository->find($id);
  }

  /**
   * Get a list of Regions.
   *
   * @param int $limit  the limit of the result
   * @param int $offset starting from the offset
   *
   * @return array
   */
  public function all($limit = 5, $offset = 0)
  {
      return $this->repository->findAll();
  }
  
  /**
   * Get a list of Farmers.
   *
   * @return array
   */
  public function allDataQuery()
  {
      return $this->repository->getAll();
  }

  /**
   * Create a new Region.
   *
   * @param array $parameters
   *
   * @return RegionInterface
   */
  public function post(array $parameters)
  {
      $region = $this->createRegion();

      return $this->processForm($region, $parameters, 'POST');
  }

  /**
   * Edit a Region.
   *
   * @param RegionInterface $region
   * @param array         $parameters
   *
   * @return RegionInterface
   */
  public function put(RegionInterface $region, array $parameters)
  {
    return $this->processForm($region, $parameters, 'PUT');
  }

  /**
   * Partially update a Region.
   *
   * @param RegionInterface $region
   * @param array         $parameters
   *
   * @return RegionInterface
   */
  public function patch(RegionInterface $region, array $parameters)
  {
      return $this->processForm($region, $parameters, 'PATCH');
  }

  /**
   * Delete a Region.
   *
   * @return boolean
   */
  public function delete($id)
  {
      $entity = $this->repository->find($id);
      if(is_object($entity)){
        $this->om->remove($entity);
        $this->om->flush();
        return true;
      }else{
        return false;
      }
  }

  /**
   * Processes the form.
   *
   * @param RegionInterface $region
   * @param array         $parameters
   * @param String        $method
   *
   * @return RegionInterface
   *
   * @throws Sky\LocationBundle\Exception\InvalidFormException
   */
  private function processForm(RegionInterface $region, array $parameters, $method = "PUT")
  {
      $form = $this->formFactory->create(new RegionType(), $region, array('method' => $method));
      if(array_key_exists('_method', $parameters))
        unset($parameters['_method']);
      $form->submit($parameters, 'PATCH' !== $method);
      if ($form->isValid()) {

          $region = $form->getData();
          $this->om->persist($region);
          $this->om->flush($region);

          return $region;
      }

      throw new InvalidFormException('Invalid submitted data', $form, $region);
  }

  private function createRegion()
  {
      return new $this->entityClass();
  }
  
  public function getDataForSync(){
    if(array_key_exists('softdeleteable',$this->om->getFilters()->getEnabledFilters())){
        $this->om->getFilters()->disable('softdeleteable');
    }
    return$this->repository->getDataForSync();
  }
}
