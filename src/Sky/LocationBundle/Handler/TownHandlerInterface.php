<?php

namespace Sky\LocationBundle\Handler;

use Sky\LocationBundle\Model\TownInterface;


interface TownHandlerInterface {

  /**
   * Get a Town given the identifier
   *
   * @api
   *
   * @param mixed $id
   *
   * @return TownInterface
   */
  public function get($id);
  

  /**
   * Get a list of Towns.
   *
   * @param int $limit  the limit of the result
   * @param int $offset starting from the offset
   *
   * @return array
   */
  public function all($limit = 5, $offset = 0);

  /**
   * Post Town, creates a new Town.
   *
   * @api
   *
   * @param array $parameters
   *
   * @return TownInterface
   */
  public function post(array $parameters);

  /**
   * Edit a Town.
   *
   * @api
   *
   * @param TownInterface   $town
   * @param array           $parameters
   *
   * @return TownInterface
   */
  public function put(TownInterface $town, array $parameters);

  /**
   * Partially update a Town.
   *
   * @api
   *
   * @param TownInterface   $town
   * @param array           $parameters
   *
   * @return TownInterface
   */
  public function patch(TownInterface $town, array $parameters);
}
