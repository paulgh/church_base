<?php

namespace Sky\LocationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TownAdmin extends Admin {

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('id')
                ->add('name')
                ->add('catchment')
//            ->add('createdAt')
//            ->add('updatedAt')
//            ->add('deletedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
//            ->add('id')
                ->add('name')
                ->add('region')
                ->add('catchment',null,array('editable'=>true))

//            ->add('updatedAt')
//            ->add('deletedAt')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
//            ->add('id')
                ->add('name')
                ->add('region')
                ->add('catchment')
                ->add('latlng', 'oh_google_maps', array(
                    'attr' => array('style' => 'margin-bottom:1.423%;'),
                    'required' => false,
                    'label' => 'search here',
                    'lat_options' => array(
                        'label' => 'Latitude',
                        'required' => false,
                        'attr' => array(
                            'style' => ''
                        )), // the options for just the lat field
                    'lng_options' => array('required' => false, 'label' => 'Longitude',
                        'attr' => array())
                    , // the options for just the lng field
                    'map_width' => 90,
                    'default_lat' => 5.652623, // the starting position on the map
                    'default_lng' => -0.209072,
                        )
                )
//            ->add('updatedAt')
//            ->add('deletedAt')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper) {
        $showMapper
                ->add('id')
                ->add('name')
                ->add('createdAt')
                ->add('updatedAt')
                ->add('deletedAt')
        ;
    }

}
