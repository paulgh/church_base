var map;
var markers = [];
var locationSelect;
function load() {
    var center; 
    map = new google.maps.Map(document.getElementById("map"), {
      center: new google.maps.LatLng(6.926426847059551,-0.7470703125),
      zoom: 7,
      mapTypeId: 'roadmap',
      mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
    });
    var lat = $('#latitude').val();
    var lng = $('#longitude').val();

    if(lat !='' && lng != ''){
        var latlng = new google.maps.LatLng(
                parseFloat(lat),
                parseFloat(lng));
        createMarker(latlng); 
        }
        
    
    google.maps.event.addListener(map, 'click', function(event) {
        clearLocations();
        createMarker(event.latLng);
        
    });
}
function clearLocations() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers.length = 0;
    
}
function createMarker(location) {
     
    var marker = new google.maps.Marker({
        map: map,
        position: location,
    });
    markers.push(marker);
    var latt = location.lat();
    var lngg = location.lng();
    $('#latitude').val(latt);
    $('#longitude').val(lngg);
}
